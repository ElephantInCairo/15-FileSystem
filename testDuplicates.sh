./mi_mkfs ha 204800

#ESPERADO, existe ruta, no existe la entrada
echo "V: creamos un directorio y un fichero"
./mi_mkdir ha 7 /dir/
./mi_touch ha 7 /file
./mi_ls    ha /
echo ""

#ESPERADO, existe ruta, no existe la entrada
echo "V: creamos dentro de una ruta"
./mi_mkdir ha 7 /dir/sdir/
./mi_touch ha 7 /dir/sfile
./mi_ls    ha /dir/
echo ""

#NO_ESPERADO, existe ruta, ya existe la entrada
echo "F: intentamos crear un directorio y un fichero que ya existen"
./mi_mkdir ha 7 /dir/sdir/
./mi_touch ha 7 /dir/sfile
./mi_ls    ha /
echo ""

#NO_ESPERADO, no existe ruta
echo "F: La ruta intermedia no existe"
./mi_mkdir ha 7 /dir/no_found/subdir/
./mi_touch ha 7 /no_found/file
echo ""

#ESPERADO, creamos un enlace simbolico
echo "V: creamos un enlace"
./mi_ln ha /file /dir/sdir/enlace1
./mi_ls ha /dir/sdir/
echo ""

#NO_ESPERADO, ya existia la entrada
echo "F: creamos un enlace, ya existía la entrada"
./mi_ln ha /file /dir/sdir/enlace1
./mi_ls ha /dir/sdir/
echo ""

#NO_ESPERADO, no existia el fichero
echo "F: no existe el fichero"
./mi_ln ha /file /dir/sdir/enlace1
./mi_ls ha /dir/sdir/
echo ""
