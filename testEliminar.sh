./mi_mkfs ha 204800

#no se puede eliminar si el directorio no esta vacio

#creamos algunos directorios y ficheros
echo "Creamos primero algunos directorios y ficheros"
./mi_mkdir ha 7 /dir1/
./mi_mkdir ha 7 /dir2/
./mi_touch ha 7 /file1
./mi_touch ha 7 /file2
./mi_escribir ha /file2 "$(cat obj/bloques.c)"
./mi_ln    ha /file2 /file3
./mi_ls    ha /
echo ""

#comprobamos que tienen la misma info
echo "./mi_cat ha /file2"
./mi_cat ha /file2
echo ""

echo "./mi_cat ha /file3"
./mi_cat ha /file3
echo ""

#eliminamos uno de los enlaces
echo "Eliminamos /file2"
./mi_rm ha /file2
./mi_ls ha /
echo ""

#comprobamos que el otro sigue teniendo la misma info
echo "./mi_cat ha /file3"
./mi_cat ha /file3
echo ""

#eliminamos el último archivo enlazado
echo "Eliminamos /file3"
./mi_rm ha /file3
./mi_ls ha /
echo ""

#eliminamos un fichero y un directorio
echo "Eliminamos /file1 y /dir1/"
./mi_rm    ha /file1
./mi_rmdir ha /dir1/
./mi_ls    ha /
echo ""
