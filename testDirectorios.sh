./mi_mkfs ha 204800

#creamos 3 dir en la raiz
echo "V: Creamos 4 directorios y dos ficheros en la raíz"
./mi_mkdir ha 7 /dir1/
./mi_mkdir ha 6 /dir2/
./mi_mkdir ha 5 /dir3/
./mi_mkdir ha 4 /dir4/
./mi_touch ha 4 /file1
./mi_touch ha 2 /file2
./mi_ls    ha /
echo ""

#creamos un dir y un fichero en dir1
echo "V: Creamos un directorio y un fichero en /dir1/"
./mi_touch ha 7 /dir1/file21
./mi_mkdir ha 7 /dir1/sub1/
./mi_ls    ha /dir1/
echo ""

#creamos un dir y un fichero en sub1
echo "V: Creamos un directorio y un fichero en /dir1/sub1/"
./mi_touch ha 7 /dir1/sub1/file22
./mi_mkdir ha 7 /dir1/sub1/sub2/
./mi_ls    ha /dir1/sub1/
echo ""

#intentamos crear un directorio en un fichero
echo "F: Intentamos crear un directorio en un fichero"
./mi_mkdir ha 7 /dir1/sub1/file22
echo ""
