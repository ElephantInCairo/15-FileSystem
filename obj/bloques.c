#include "bloques.h"

//variables
static int descriptor = 0;


/**
 * Abre el fichero indicado por parámetro
 *  usará dicho fichero como disco.imagen
 *  de nuestro sistema de ficheros.
 * Retorna el descriptor o -1 en caso de fallo.
 */
int bmount (const char * camino) {
    initSem();
    descriptor = open(camino, PERM_HD, MODE_HD);
    if( descriptor == -1 ) return error(DESCRIPTOR);
    return descriptor;
}

/**
 * Cierra de forma segura el sistema de
 *  ficheros.
 * Retorna:
 *  -  0: si la operación se ha realizado
 *  correctamente
 *  - -1: en caso de fallo
 */
int bumount() {
    int status = close( descriptor );
    deleteSem();
    return status;
}

/**
 * Escribe el contenido de un búffer de memoria
 *  (dicho buffer tendrá el tamaño de un bloque)
 *  en la posición equivalente del SF, dicha
 *  posición se indica en el primer parámetro
 *  de la llamada.
 * Retorna en nº de bytes que ha podido escribir
 *  o -1 en caso de error
 */
int bwrite(unsigned int nblock, const void *buf) {
    off_t dist;
    dist = lseek(descriptor, nblock*BLOCKSIZE, SEEK_SET);
    if (dist == -1) return -1;

    size_t w_bytes;
    w_bytes = write(descriptor, buf, BLOCKSIZE);
    return w_bytes;
}

/**
 * Lee del SF el bloque especificado y copia su contenido
 *  en el búffer
 * El puntero del fichero queda indicando el próximo byte
 *  a leer.
 * Retorna el nº de bytes leidos o -1 en caso de error
 *
 */
int bread(unsigned int nblock, void *buf) {
    off_t dist;
    dist = lseek(descriptor, nblock*BLOCKSIZE, SEEK_SET);
    if (dist == -1) return -1;

    size_t r_bytes;
    r_bytes = read(descriptor, buf, BLOCKSIZE);
    return r_bytes;
}

