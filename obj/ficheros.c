#include <stdio.h>
#include "ficheros.h"

int mi_write_f (unsigned int ninodo, const void *buf_original, unsigned int offset, unsigned int nbytes)
{
    //leemos el inodo
    Inodo inodo;
    inodo = leer_inodo(ninodo);

    //comprobamos los permisos
    if( (inodo.permisos & P_ESCR) == 0) return error(PERM_ESCR);

    //número de bloque lógico y desplazamiento del búffer
    unsigned int bLogico      = offset/BLOCKSIZE;
    unsigned int desplEnt     = 0;
    unsigned char *bloqueEnt  = (unsigned char*) buf_original;

    //número de bloque físico y desplazamiento en el bloque
    unsigned char bloqueSF[BLOCKSIZE];
    int bFisico;

    //rango de información útil en el bloque
    unsigned int desplIniBloque = offset%BLOCKSIZE;
    unsigned int desplFinBloque = (desplIniBloque+nbytes<BLOCKSIZE) ? desplIniBloque+nbytes : BLOCKSIZE;

    unsigned char *bloque;
    Iterador_Inodo it;
    obt_iin(bLogico, &it);
    while (desplIniBloque<desplFinBloque) {
	if (!es_iin_val(&it)) return error(OVERFLOW_FICHERO);
	bFisico = cons_iin(ninodo, &it, OP_ESCRITURA);
        if (desplIniBloque>0 || desplFinBloque<BLOCKSIZE-1) {
            bread(bFisico, bloqueSF);
            memcpy(bloqueSF+desplIniBloque, bloqueEnt+desplEnt, desplFinBloque-desplIniBloque);
            bloque = bloqueSF;
        } else {
            bloque = bloqueEnt+desplEnt;
        }
        //mi_waitSem(); bwrite(bFisico, bloque); mi_signalSem();
        bwrite(bFisico, bloque);
        desplEnt      += desplFinBloque-desplIniBloque;
        desplIniBloque = 0;
        desplFinBloque = (nbytes-desplEnt<BLOCKSIZE) ? nbytes-desplEnt : BLOCKSIZE;
        sig_iin_datos(&it);
    }

    mi_waitSem();
    inodo = leer_inodo(ninodo);
    unsigned int ultimoByte = offset+nbytes;
    if (inodo.tamEnBytesLog < ultimoByte) {
        inodo.tamEnBytesLog = ultimoByte;
        escribir_inodo(ninodo, inodo);
    }
    mi_signalSem();

    return desplEnt;
}

int mi_read_f (unsigned int ninodo, void * buf_original, unsigned int offset, unsigned int nbytes)
{
    //obtenemos el inodo
    Inodo inodo;
    inodo = leer_inodo(ninodo);

    //permisos
    if( (inodo.permisos & P_LECT) == 0) return error(PERM_LECT);

    //ajustar el nbytes que vamos a poder leer
    unsigned int f_size = inodo.tamEnBytesLog;
    if      ( offset        > f_size ) nbytes = 0;
    else if ( offset+nbytes > f_size ) nbytes = f_size-offset;

    //número de bloque lógico y desplazamiento del búffer
    unsigned int bLogico      = offset/BLOCKSIZE;
    unsigned int desplEnt     = 0;
    unsigned char *bloqueEnt  = (unsigned char*) buf_original;

    //número de bloque físico y desplazamiento en el bloque
    int bFisico;
    unsigned char bloqueSF[BLOCKSIZE];

    //rango de información útil en el bloque
    unsigned int desplIniBloque = offset%BLOCKSIZE;
    unsigned int desplFinBloque = (desplIniBloque+nbytes<BLOCKSIZE) ? desplIniBloque+nbytes : BLOCKSIZE;

    Iterador_Inodo it;
    obt_iin(bLogico, &it);
    while (desplIniBloque<desplFinBloque) {
	if (!es_iin_val(&it)) return error(OVERFLOW_FICHERO);
	bFisico = cons_iin(ninodo, &it, OP_LECTURA);
	if (bFisico == NULL_BLOQUE_W) return NULL_BLOQUE_W;
	if (desplIniBloque>0 || desplFinBloque<BLOCKSIZE-1) {
	    bread(bFisico, bloqueSF);
	    memcpy(bloqueEnt+desplEnt, bloqueSF+desplIniBloque, desplFinBloque-desplIniBloque);
	} else {
	    if (bFisico == 0) memset(bloqueEnt+desplEnt, 0, BLOCKSIZE);
	    else bread(bFisico, bloqueEnt+desplEnt);
	}
        desplEnt += desplFinBloque-desplIniBloque;
        desplIniBloque = 0;
        desplFinBloque = (nbytes-desplEnt<BLOCKSIZE) ? nbytes-desplEnt : BLOCKSIZE;
	sig_iin_datos(&it);
    }

    return nbytes;
}


int mi_chmod_f (unsigned int ninodo, unsigned char permisos0)
{
    mi_waitSem();

    //obtenemos el inodo
    Inodo inodo;
    inodo = leer_inodo(ninodo);

    //actualizamos los datos del inodo
    inodo.ctime    = time(NULL);
    inodo.permisos = permisos0;

    //escribimos el inodo de nuevo en el SF
    escribir_inodo(ninodo, inodo);

    mi_signalSem();
    return 0;
}


int mi_truncar_f (unsigned int ninodo, unsigned int nbytes)
{
    mi_waitSem();

    //Obtenemos un inodo (obtenemos el tamaño del inodo)
    Inodo inodo;
    inodo = leer_inodo(ninodo);

    //obtenemos los límites del rango que vamos a liberar
    unsigned int bLogico    = nbytes              / BLOCKSIZE;
    unsigned int bLogicoFin = inodo.tamEnBytesLog / BLOCKSIZE;
    if (nbytes%BLOCKSIZE>0) bLogico++;

    //pre-actualizamos
    inodo.tamEnBytesLog = nbytes;
    escribir_inodo(ninodo, inodo);

    mi_signalSem();

    //se liberarán los inodos a partir del bloque siguiente al truncado
    liberar_bloques_inodo(ninodo, bLogico, bLogicoFin);

    mi_waitSem();

    //actualización de los datos del inodo
    inodo = leer_inodo(ninodo);
    inodo.ctime = time(NULL);

    //escribimos el inodo de nuevo en el SF
    escribir_inodo (ninodo, inodo);

    mi_signalSem();
    return 0;
}

int mi_stat_f(unsigned int ninodo, p_Stat p_stat)
{
    //obtenemos el inodo
    Inodo inodo;
    inodo = leer_inodo(ninodo);

    //Recogemos el estado del inodo en la estructura STAT
    p_stat->tipo  = inodo.tipo;
    p_stat->atime = inodo.atime;
    p_stat->mtime = inodo.mtime;
    p_stat->ctime = inodo.ctime;
    p_stat->nlinks             = inodo.nlinks;
    p_stat->permisos           = inodo.permisos;
    p_stat->tamEnBytesLog      = inodo.tamEnBytesLog;
    p_stat->numBloquesOcupados = inodo.numBloquesOcupados;

    return 0;
}

int mi_link_f(unsigned int ninodo)
{
    mi_waitSem();

    Inodo inodo;
    inodo = leer_inodo(ninodo);
    inodo.nlinks++;

    escribir_inodo(ninodo, inodo);

    mi_signalSem();
    return 0;
}

int mi_unlink_f(unsigned int ninodo)
{
    int status;
    mi_waitSem();

    Inodo inodo;
    inodo = leer_inodo(ninodo);

    if (inodo.nlinks == 0) status = liberar_inodo(ninodo);
    else {
	inodo.nlinks--;
	status = escribir_inodo(ninodo, inodo);
    }

    mi_signalSem();
    return status;
}
