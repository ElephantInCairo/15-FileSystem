#include "bloques.h"

//reservar_bloque
#define OP_LECTURA     1
#define OP_ESCRITURA   2
#define OP_ELIMINACION 4

//distinciones al tratar una ruta
#define BLOQUE 1
#define INODO  2
#define HOJAS  4
#define ARBOL  8

//inodo.tipo
#define DIRECTORIO 'd'
#define FICHERO    'f'
#define LIBRE      'l'
#define MIXTO      'm'

//inodo.permisos
#define P_LECT 4  /* r */
#define P_ESCR 2  /* w */
#define P_EJEC 1  /* x */

//tamaño en bytes de un inodo
#define T_INODO 128
#define T_ITER_IN sizeof(struct iterador_inodo_s)

//num_punteros directos
#define N_PTR_DIR   12
#define N_PTR_INDIR  3
#define N_PTR        (N_PTR_DIR+N_PTR_INDIR)
#define PTR_DIR_MAX    (N_PTR_DIR   -1)
#define PTR_INDIR_MAX  (N_PTR_INDIR -1)

/* campo pre-calculado */
#define FICHERO_MAX  16843019


//estructuras
struct inodo_s
{
        time_t atime; //8bytes
        time_t mtime;
        time_t ctime;

        unsigned int nlinks; //4bytes
        unsigned int tamEnBytesLog;
        unsigned int numBloquesOcupados;

        unsigned int punteros[N_PTR];

        unsigned char tipo; //1byte
        unsigned char permisos;

        unsigned char padding[T_INODO-(38+4*(N_PTR))];
};

struct iterador_inodo_s
{
        unsigned char v_entradas[5];
        unsigned char idxStack;
};

typedef int (*p_func)(int, int, unsigned int*, void*, char);
//rutaLogica
typedef struct iterador_inodo_s Iterador_Inodo;
typedef struct iterador_inodo_s * p_Iterador_Inodo;
//inodo
typedef struct inodo_s Inodo;
typedef struct inodo_s * p_Inodo;


//implementadas en ficheros_basico.c
int reservar_bloque();
int liberar_bloque (int nbloque);
int escribir_inodo (int ninodo, Inodo inodo);
Inodo leer_inodo (int ninodo);


int  obt_iin        (unsigned int nblogico, p_Iterador_Inodo ruta);
int  cons_iin       (int ninodo, p_Iterador_Inodo ruta, char operacion);
char sig_iin_arbol  (p_Iterador_Inodo ruta);
char sig_iin_datos  (p_Iterador_Inodo ruta);
char es_iin_val     (p_Iterador_Inodo ruta);
char es_iin_val_rango (p_Iterador_Inodo iniRuta, p_Iterador_Inodo finRuta);
void obt_fin_iin    (p_Iterador_Inodo it);
void print_iin      (p_Iterador_Inodo ruta);
