#include <stdio.h>
#include "directorios.h"

#define T_STIME     50
#define ENTRADA_MAX (BLOCKSIZE/sizeof(struct entrada_s))
#define T_ENTRADA              sizeof(struct entrada_s)

int crear_entrada_dir(p_Entrada out_entrada, char * nombre, char tipo, unsigned char permisos);
int escribir_entrada(int ninodo, unsigned int offset, p_Entrada entrada);
int eliminar_entrada(int ninodo, unsigned int entrada);
int buscar_nombre   (int * ninodo, char * nombre0, char existe, unsigned int * p_entrada);
void mostrar_info   (int ninodo);


/**
 * ERROR: No se ha encontrado el camino
 * ERROR: El tipo de inodo no coincide
 */
int obtener_inodo (const char * camino, int * p_inodo, char t_inodo0)
{
    int status = 0;

    //parámetros buscar_nombre
    char buffer[60];
    char * nombre = buffer;
    char t_inodo  = DIRECTORIO;
    unsigned int nentrada;

    Iterador_Camino it;
    primero(camino, &it);
    while (es_cam_val(camino, &it) && status==0) {
        cons_and_sig(camino, &it, nombre, &t_inodo);
        status = buscar_nombre(p_inodo, nombre, TRUE, &nentrada);
    }

    if (t_inodo0 != MIXTO)
        if (t_inodo0 != t_inodo)
            status = error(TIPO_INODO_NO_ESPERADO);

    return status;
}

/**
 * ERROR: no se ha encontrado (debía estar)
 * ERROR: ya existe (no debería estar)
 * ERROR: el tipo de inodo no coincide
 */
int obtener_inodo_padre (const char * camino, int * p_inodo, char existe, char t_inodo0, unsigned int * p_entrada, char * nombre, int * p_inodo_hijo)
{
    int status = 0;

    //parámetros buscar_nombre
    char t_inodo;

    Iterador_Camino it;
    primero(camino, &it);
    while (es_dir_padre_valido(camino, &it) && status==0) {
        cons_and_sig(camino, &it, nombre, &t_inodo);
        status = buscar_nombre(p_inodo, nombre, TRUE, p_entrada);
    }
    if (status<0) return status;

    *p_inodo_hijo = *p_inodo;
    cons_and_sig (camino, &it, nombre, &t_inodo);
    status = buscar_nombre (p_inodo_hijo, nombre, existe, p_entrada);

    if (t_inodo != t_inodo0) status = error(TIPO_INODO_NO_ESPERADO);

    return status;
}

/**
 * ninodo: inodo del directorio en el que vamos a buscar la entrada
 *  y donde vamos a guardar el valor del inodo que buscabamos en caso
 *  de encontrarlo, sinó lo deja intacto
 * nombre0: nombre de la entrada que buscamos
 * encontrado: si dicho nombre estaba en el directorio
 * p_entrada: número de entrada que ocupa
 */
int buscar_nombre(int * p_inodo_dir, char * nombre0, char existe, unsigned int * p_entrada)
{
    //Obtenemos un bloque (vamos a leer el inodo y el vector de entradas)
    unsigned char buffer[BLOCKSIZE];
    p_Entrada entrada = (p_Entrada) buffer;
    p_Inodo     inodo = (p_Inodo)   buffer;

    //Comprobamos permisos y obtenemos el num_entradas
    *inodo = leer_inodo(*p_inodo_dir);
    //if ( (inodo->permisos & P_EJEC) == 0) return error(PERM_EJEC);
    unsigned int entradaLim = inodo->tamEnBytesLog/T_ENTRADA;

    mi_waitSem();
    //Buscamos el nombre0
    char * nombre;
    char encontrado = FALSE;
    unsigned int idxEntrada;
    for (idxEntrada=0; idxEntrada<entradaLim && !encontrado; idxEntrada++) {
        //si estamos sin entradas, leemos un bloque de entradas
        if ((idxEntrada%ENTRADA_MAX)==0)
            mi_read_f(*p_inodo_dir, entrada, idxEntrada*(T_ENTRADA), BLOCKSIZE);
        //cogemos el nombre y comparamos
        nombre     = entrada[idxEntrada%ENTRADA_MAX].nombre;
        encontrado = !strcmp(nombre, nombre0);
    }
    mi_signalSem();

    //errores
    if (encontrado != existe) {
        if (encontrado)  return error(YA_EXISTE); else
        if (!encontrado) return error(NO_EXISTE);
    }

    //ajustamos valores
    if (encontrado) {
        idxEntrada--;
        *p_inodo_dir = entrada[idxEntrada%ENTRADA_MAX].inodo;
        *p_entrada   = idxEntrada;
    }
    else *p_entrada = idxEntrada;


    return 0;
}


int mi_create (const char * camino, unsigned char permisos, char t_inodo0)
{
    mi_waitSem();
    int status = 0;

    //datos del inodo padre
    int ninodo = 0;
    int ninodo_hijo;
    unsigned int nentrada;

    char nombre[60];
    status = obtener_inodo_padre(camino, &ninodo, FALSE, t_inodo0, &nentrada, nombre, &ninodo_hijo);
    if (status<0) { mi_signalSem(); return status; }

    //escribiendo 0 bytes comprobamos los permisos
    status = mi_write_f(ninodo, NULL, 0, 0);
    if (status<0) { mi_signalSem(); return status; }

    Entrada entrada;
    status = crear_entrada_dir (&entrada, nombre, t_inodo0, permisos);
    escribir_entrada (ninodo, nentrada*T_ENTRADA, &entrada);

    mi_signalSem();
    return status;
}

/**
 * Razón del BigLock: parámetro puntero a búffer de salida
 */
int mi_dir (const char * camino, char * buffer)
{
    int status;
    mi_waitSem();

    //datos del inodo
    int ninodo = 0;
    status = obtener_inodo(camino, &ninodo, DIRECTORIO);
    if (status<0) return status;

    Inodo inodo;
    inodo = leer_inodo(ninodo);

    //escribiendo 0 bytes comprobamos los permisos
    status = mi_read_f(ninodo, NULL, 0, 0);
    if (status<0) return status;

    char * p_nombre;
    unsigned int ninodo_ent;
    unsigned int idxEntrada;
    unsigned int entradaLim = inodo.tamEnBytesLog/T_ENTRADA;
    Entrada entrada[ENTRADA_MAX];
    for (idxEntrada=0; idxEntrada<entradaLim; idxEntrada++) {
        if ((idxEntrada%ENTRADA_MAX)==0)
	    mi_read_f(ninodo, entrada, idxEntrada*(T_ENTRADA), BLOCKSIZE);
        p_nombre   = entrada[idxEntrada%ENTRADA_MAX].nombre;
        ninodo_ent = entrada[idxEntrada%ENTRADA_MAX].inodo;
        printf("%s", p_nombre); mostrar_info(ninodo_ent);
    }
    mi_signalSem();

    return idxEntrada;
}

int mi_link (const char * camino1, const char * camino2)
{
    int status;
    mi_waitSem();

    //camino1
    int ninodo = 0;
    status = obtener_inodo(camino1, &ninodo, FICHERO);
    if (status<0) { mi_signalSem(); return status; }

    //camino2
    char nombre[60];
    unsigned int nentrada;
    int ninodo_link = 0;
    int ninodo_hijo;
    status = obtener_inodo_padre(camino2, &ninodo_link, FALSE, FICHERO, &nentrada, nombre, &ninodo_hijo);
    if (status<0) { mi_signalSem(); return status; }

    mi_link_f(ninodo);

    //crear entrada y escribirla
    Entrada entrada;
    strncpy(entrada.nombre, nombre, 60);
    entrada.inodo = ninodo;
    escribir_entrada(ninodo_link, nentrada*T_ENTRADA, &entrada);

    mi_signalSem();
    return 0;
}


int mi_unlink (const char * camino, char t_inodo0)
{
    int status;
    mi_waitSem();

    //datos del inodo padre
    int ninodo = 0;
    int ninodo_hijo;
    unsigned int nentrada;

    char nombre[60];
    status = obtener_inodo_padre(camino, &ninodo, TRUE, t_inodo0, &nentrada, nombre, &ninodo_hijo);
    if (status<0) { mi_signalSem(); return status; }

    eliminar_entrada(ninodo, nentrada);
    status = mi_unlink_f(ninodo_hijo);

    mi_signalSem();
    return status;
}


int mi_chmod(const char * camino, unsigned char permisos)
{
    int status;

    //datos del inodo
    int ninodo = 0;
    status = obtener_inodo(camino, &ninodo, MIXTO);
    if (status<0) return status;

    status = mi_chmod_f(ninodo, permisos);
    return status;
}

int mi_stat (const char * camino, p_Stat stat)
{
    int status;

    //datos del inodo
    int ninodo = 0;
    status = obtener_inodo(camino, &ninodo, MIXTO);
    if (status<0) return status;

    status = mi_stat_f(ninodo, stat);
    return status;
}


int mi_read (const char * camino, void * buf, unsigned int offset, unsigned int nbytes)
{
    int status;

    //datos del inodo
    int ninodo = 0;
    status = obtener_inodo(camino, &ninodo, FICHERO);
    if (status<0) return status;

    status = mi_read_f(ninodo, buf, offset, nbytes);
    return status;
}

/**
 * Es necesario usar BigLock aquí solo porque el parámetro buf es un puntero
 */
int mi_write (const char * camino, const void * buf, unsigned int offset, unsigned int nbytes)
{
    int status;
    mi_waitSem();

    //datos del inodo
    int ninodo = 0;
    status = obtener_inodo(camino, &ninodo, FICHERO);
    if (status<0) return status;

    status = mi_write_f(ninodo, buf, offset, nbytes);
    mi_signalSem();
    return status;
}


/**
 * PREC: solo crea la entrada si no existe ya en el directorio en el que la vamos a insertar
 */
int crear_entrada_dir(p_Entrada out_entrada, char * nombre, char tipo, unsigned char permisos)
{
    int inodoEntr = reservar_inodo(tipo, permisos);
    if (inodoEntr<0) return OVERFLOW_INODOS;

    strncpy(out_entrada->nombre, nombre, 60);
    out_entrada->inodo = inodoEntr;
    return 0;
}

int eliminar_entrada(int ninodo, unsigned int nentrada)
{
    Inodo inodo;
    inodo = leer_inodo(ninodo);

    int ultimaEntrada = inodo.tamEnBytesLog-T_ENTRADA;

    //la movemos al final
    if (ultimaEntrada/T_ENTRADA > nentrada) {
	Entrada entrada;
	const void * p_entrada = &entrada;

	//leemos la última
	mi_read_f(ninodo, &entrada, ultimaEntrada, T_ENTRADA);

	//sobreescribimos la que eliminamos
	mi_write_f(ninodo, p_entrada, nentrada*T_ENTRADA, T_ENTRADA);
    }

    //recortamos la ultima que esta duplicada
    mi_truncar_f(ninodo, ultimaEntrada);
    return 0;
}

//esto ya debería modificar el tamaño
inline int escribir_entrada(int ninodo, unsigned int offset, p_Entrada entrada)
{
    return mi_write_f(ninodo, entrada, offset, T_ENTRADA);
}

inline void mostrar_info(int ninodo)
{
    char atime[T_STIME];
    char * p_atime = atime;

    Inodo inodo;
    inodo = leer_inodo(ninodo);

    //tipo
    printf("\t");
    if      (inodo.tipo == DIRECTORIO) printf("d");
    else if (inodo.tipo == FICHERO)    printf("f");
    else                               printf("-");

    //permisos
    if (inodo.permisos & P_LECT) printf("r"); else printf("-");
    if (inodo.permisos & P_ESCR) printf("w"); else printf("-");
    if (inodo.permisos & P_EJEC) printf("x"); else printf("-");

    //ninodo
    printf("\t%d", ninodo);

    //tamaño
    printf("\t%d", inodo.tamEnBytesLog);

    //fecha
    strftime(p_atime, T_STIME, "%a %Y­%m­%d %H:%M:%S", localtime(&inodo.atime));
    printf("\t%s\n", p_atime);
}
