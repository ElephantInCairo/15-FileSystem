#include "directorios.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define T_STIME 60


/**
 * Primer argumento  (1): nombre del SF
 * Segundo argumento (2): ruta
 */
int main(int argc, char *argv[]) {

    char * nombre_sf;
    char * ruta;
    char atime[T_STIME];
    char mtime[T_STIME];
    char ctime[T_STIME];
    char * p_atime = atime;
    char * p_mtime = mtime;
    char * p_ctime = ctime;
    Stat stat;

    //confirmar que los argumentos estan bien
    if(argc != 3) {
        printf("Formato: %s nombre_sf /ruta\n", argv[0]);
        return error(NUMERO_PARAMETROS);
    }

    //ponemos nombres legibles
    nombre_sf = argv[1];
    ruta      = argv[2];

    //montamos el sistema de ficheros
    bmount(nombre_sf);

    //obtenemos info
    mi_stat(ruta, &stat);

    //tipo y permisos
    printf("%c", stat.tipo);
    if (stat.permisos & P_LECT) printf("r"); else printf("-");
    if (stat.permisos & P_ESCR) printf("w"); else printf("-");
    if (stat.permisos & P_EJEC) printf("x"); else printf("-");

    //nlinks, tam, tamEnBloquesOcupados
    printf("\t");
    printf("%d\t", stat.nlinks);
    printf("%d\t", stat.tamEnBytesLog);
    printf("%d\n", stat.numBloquesOcupados);

    //marcas de tiempo
    strftime(p_atime, T_STIME, "%a %Y­%m­%d %H:%M:%S", localtime(&stat.atime));
    strftime(p_mtime, T_STIME, "%a %Y­%m­%d %H:%M:%S", localtime(&stat.mtime));
    strftime(p_ctime, T_STIME, "%a %Y­%m­%d %H:%M:%S", localtime(&stat.ctime));
    printf("%s\n", p_atime);
    printf("%s\n", p_mtime);
    printf("%s\n", p_ctime);

    //desmontamos el sistema de ficheros
    bumount();

    return 0;
}
