/* fichero ficheros_basico.h */
#include <time.h>
#include <limits.h>
#include "iterador_inodo.h"

//el superbloque se escribe en el primer bloque de nuestro FS
#define BLOCK_SB  0

#define LIBERAR  0
#define RESERVAR 1


struct superbloque
{
    unsigned int posPrimerBloqueMB;
    unsigned int posUltimoBloqueMB;

    unsigned int posPrimerBloqueAI;
    unsigned int posUltimoBloqueAI;

    unsigned int posPrimerBloqueDatos;
    unsigned int posUltimoBloqueDatos;

    unsigned int posInodoRaiz;
    unsigned int posPrimerInodoLibre;

    unsigned int cantBloquesLibres;
    unsigned int cantInodosLibres;

    unsigned int totBloques;
    unsigned int totInodos;

    char padding[BLOCKSIZE-12*sizeof(unsigned int)];
};


int tamMB (unsigned int nbloques);
int tamAI (unsigned int ninodos);
int initSB (unsigned int nbloques, unsigned int ninodos);
int initMB (unsigned int nbloques);
int initAI (unsigned int ninodos);

int escribir_bit (int nbloque, char bit);
char leer_bit    (int nbloque);

int reservar_inodo(unsigned char tipo, unsigned char permisos);
int liberar_inodo (int ninodo);

int liberar_bloques_inodo(int ninodo, unsigned int bLogico, unsigned int bLogicoFin);

/* declaradas en iterador_inodo.h
int reservar_bloque();
int liberar_bloque (int nbloque);
int escribir_inodo (int ninodo, Inodo inodo);
Inodo leer_inodo   (int ninodo);
*/
