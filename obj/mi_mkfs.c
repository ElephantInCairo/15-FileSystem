#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "ficheros_basico.h"

int main(int argc, char **argv) {

    if(argc != 3) {
	printf("Formato: %s nombre_sf tamaño\n", argv[0]);
        return -1;
    }

    char *nom_hd   = argv[1];
    int   nbloques = atoi(argv[2]); //en bloques
    int   ninodos  = nbloques/4;

    bmount(nom_hd);

    initSB(nbloques, ninodos);
    initMB(nbloques);
    initAI(ninodos);
    reservar_inodo(DIRECTORIO, 7);

    bumount();
    return 0;
}
