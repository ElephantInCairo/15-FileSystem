/**
 * OBSERVACIONES:
 * Si BLOCKSIZE/sizeof(unsigned int) > 256:
 *   Se debería editar la funcion obtener_pos ya que
 *   usa unsigned char para el rango de la entrada
 * Revisar las funciones tratar (PREC/POST)
 * En el Inodo tenemos consecutivos los punteros directos
 *  e indirectos.
 */
#include <stdio.h>
#include "ficheros_basico.h"


/**
 * Tamaño del Mapa de Bits (en bloques).
 * Cada bit: indica si el bloque correspondiente
 *  está siendo usado o no.
 * En un bloque caben BLOCKSIZE*8 bits
 */
int tamMB (unsigned int nbloques) {
    int tam = (nbloques/8) / BLOCKSIZE;
    if ( (nbloques/8) % BLOCKSIZE > 0 ) tam++;
    return tam;
}

/**
 * Tamaño del Array de Inodos (en bloques).
 * Los inodos contienen la información de cada
 *  fichero almacenado.
 * Si están libres se encuentran en la pila de
 *  memoria libre.
 *  (el tamaño de un inodo es 128 bytes)
 */
int tamAI (unsigned int ninodos) {
    int tam = (ninodos*T_INODO) / BLOCKSIZE;
    if ( (ninodos*T_INODO) % BLOCKSIZE > 0 ) tam++;
    return tam;
}

/**
 * Inicializa el SuperBloque.
 * El SuperBloque (SB) contiene:
 * primer bloque MB
 * último bloque MB
 * primer bloque AI
 * último bloque AI
 * primer bloque de Datos
 * último bloque de Datos
 * inodo que apunta a raíz (/)
 * pila de inodos libres
 * nº de bloques libres
 * nº de inodos libres
 * total de bloques
 * total de inodos
 */
int initSB(unsigned int nbloques, unsigned int ninodos)
{
    struct superbloque SB;

    SB.posPrimerBloqueMB    = 1 + BLOCK_SB;
    SB.posPrimerBloqueAI    = SB.posPrimerBloqueMB + tamMB(nbloques);
    SB.posPrimerBloqueDatos = SB.posPrimerBloqueAI + tamAI(ninodos);

    SB.posUltimoBloqueMB    = SB.posPrimerBloqueAI    - 1;   //calculables
    SB.posUltimoBloqueAI    = SB.posPrimerBloqueDatos - 1;   //calculables
    SB.posUltimoBloqueDatos = nbloques                - 1;   //calculables

    //Posición en bytes desde posPrimerBloqueAI
    SB.posInodoRaiz         = 0 * T_INODO;
    SB.posPrimerInodoLibre  = 0 * T_INODO;

    SB.cantBloquesLibres    = nbloques;
    SB.cantInodosLibres     = tamAI(ninodos)*(BLOCKSIZE/T_INODO);

    SB.totBloques           = nbloques;
    SB.totInodos            = tamAI(ninodos)*(BLOCKSIZE/T_INODO);

    bwrite(BLOCK_SB, &SB);
    return 0;
}

/**
 * Inicializa el Mapa de Bits.
 * Marca todos los bits del MB (que corresponden a cada
 *  bloque de la sección de Datos) a 0, indicando que
 *  no están siendo usados.
 * Hay que marcar los bloques correspondientes a la Meta-
 *  Información como bloques en uso (marcandolos con un 1)
 */
int initMB(unsigned int nbloques)
{
    //Obtenemos un búffer (que podemos tratar como SB)
    unsigned char bloque[BLOCKSIZE];
    struct superbloque *SB = (struct superbloque*) bloque;

    //leemos el SB y obtenemos los rangos de MB y AI
    bread(BLOCK_SB, SB);
    int startMB = SB->posPrimerBloqueMB;
    int startAI = SB->posPrimerBloqueAI;
    int endMB   = SB->posUltimoBloqueMB;
    int endAI   = SB->posUltimoBloqueAI;

    //sobreescribimos la info leída (a todo 0's)
    memset(bloque, 0, BLOCKSIZE);

    //marcamos que todos los bloques están libres en el MB
    // (esto es la info de todos los bloques del MB es 0)
    int idx;
    for (idx=startMB; idx<=endMB; idx++) bwrite(idx, bloque);

    //marcamos ocupados los bits que corresponden a
    // los bloques del MB además de los del SB y AI
    // (esto es la posición del nbloque bit valdrá 1)
    escribir_bit(BLOCK_SB, 1);
    for (idx=startMB; idx<=endMB; idx++) escribir_bit(idx, 1);
    for (idx=startAI; idx<=endAI; idx++) escribir_bit(idx, 1);

    return 0;
}


/**
 * Inicializa el Array de Inodos.
 * Dado que al inicializarlos están todos libres hay que
 *  crear una estructura enlazada de inodos libres, para
 *  ello usamos el primer campo de punterosDirectos.
 */
int initAI(unsigned int ninodos)
{
    //Obtenemos un búffer (lo usaremos como SB y vector de inodos)
    unsigned char bloque[BLOCKSIZE];
    struct superbloque *SB = (struct superbloque*) bloque;
    p_Inodo AI = (p_Inodo) bloque;

    //leemos el SuperBloque. Obten: ini, fin(AI) y primer inodo libre
    bread(BLOCK_SB, SB);
    unsigned int startAI = SB->posPrimerBloqueAI;
    unsigned int endAI   = SB->posUltimoBloqueAI;
    unsigned int p_libre = SB->posPrimerInodoLibre;

    //vaciamos el búffer para rellenar solo los campos oportunos
    memset(bloque, 0, BLOCKSIZE);

    //recorremos los bloques que corresponden al AI
    // en cada bloque escribimos los inodos que caben
    // con su correspondiente info de lista enlazada
    unsigned int idxBloqueAI;
    unsigned char idxInod;
    unsigned char idxInodLast = (BLOCKSIZE/T_INODO)-1;
    //recorremos los bloques del AI
    for(idxBloqueAI=startAI; idxBloqueAI<=endAI; idxBloqueAI++) {
        //Añadimos inodos al bloque (llamado AI)
        for(idxInod=0; idxInod<=idxInodLast; idxInod++) {
            p_libre                        += T_INODO;
            AI[idxInod].tipo                = LIBRE;
            AI[idxInod].punteros[0] = p_libre;
        }
        //escribimos el bloque
        bwrite(idxBloqueAI, AI);
    }

    return 0;
}

/**
 * Dado un nbloque y la pos inicial del MB
 *  lee el bloque y devuelve punteros a:
 *  índicie de bloque, valor, desplBit.
 */
void obtener_pos(unsigned int nbloque, unsigned char * out_bloque,
	unsigned int * pos, unsigned char **valor, unsigned char *despl) {

    //pos inicialMB + su pos en el MB
    *pos += (nbloque/8) / BLOCKSIZE;
    bread(*pos, out_bloque);

    //pos dentro del bloque
    *valor += (nbloque/8)%BLOCKSIZE;
    *despl >>= nbloque%8;
}

/**
 * Localiza el bit correspondiente a nbloque (en MB)
 *  actualiza su estado (1/0) y actualiza el campo
 *  cantBloquesLibres en el SB
 */
int escribir_bit(int nbloque, char bit)
{
    //obtenemos la pos del primer bloque del MB
    struct superbloque SB;
    bread(BLOCK_SB, &SB);

    //bloque
    unsigned char bloqueMB[BLOCKSIZE];
    unsigned int  idxBloque = SB.posPrimerBloqueMB;

    //valor
    unsigned char *valor  = bloqueMB;
    unsigned char mascara = 128;

    obtener_pos(nbloque, bloqueMB, &idxBloque, &valor, &mascara);

    //si añadimos un 1: tenemos un bloque libre menos
    if (bit == RESERVAR) {
        if ( (*valor |  mascara) == *valor) return error(VALOR_INCONSISTENTE);
        SB.cantBloquesLibres--;
        *valor |=  mascara;
    // si quitamos el 1: tenemos un bloque libre más
    } else if (bit == LIBERAR) {
        if ( (*valor & ~mascara) == *valor) return error(VALOR_INCONSISTENTE);
        SB.cantBloquesLibres++;
        *valor &= ~mascara;
    }

    //escribimos los cambios
    bwrite(BLOCK_SB, &SB);
    bwrite(idxBloque, bloqueMB);

    return 0;
}

/**
 * Localiza el bloque del que queremos saber su estado y
 *  devuelve el valor de su bit correspondiente
 */
char leer_bit(int nbloque)
{
    //obtenemos la pos del primer bloque del MB
    struct superbloque SB;
    bread(BLOCK_SB, &SB);

    //bloque
    unsigned char bloqueMB[BLOCKSIZE];
    unsigned int  idxBloque = SB.posPrimerBloqueMB;

    //valor
    unsigned char *valor = bloqueMB;
    unsigned char mascara = 128;

    obtener_pos(nbloque, bloqueMB, &idxBloque, &valor, &mascara);

    //en lógica: x AND true = x;
    return (*valor & mascara) && TRUE;
}

/**
 * Encuentra el primer bloque libre,
 *  devuelve su posición y lo deja ocupado
 * Metodologia: inicializa índice, busca 0, ajusta nbloque
 */
int reservar_bloque()
{
    //Obtenemos un búffer (lo usaremos como SB)
    unsigned char bloque[BLOCKSIZE];
    struct superbloque *SB = (struct superbloque*) bloque;

    //leemos el SB, verificamos espacio disp, obtenemos los límites
    bread(BLOCK_SB, SB);
    if (SB->cantBloquesLibres == 0) return error(OVERFLOW_BLOQUES);
    unsigned int startMB = SB->posPrimerBloqueMB;
    unsigned int   endMB = SB->posUltimoBloqueMB;

    //hemos de encontrar el primer 0 en el MB,
    // usaremos un búffer de todo 1's
    unsigned char cmp[BLOCKSIZE];
    memset(cmp, UCHAR_MAX, BLOCKSIZE);

    //Vamos a ir ajustando la ubicación del bloque
    unsigned int nbloque = 0;
    unsigned int idx;

    //Ini, busca, ajusta con bloques
    idx = startMB-1; bread(++idx, bloque);
    while (memcmp(bloque, cmp, BLOCKSIZE)>=0) bread(++idx, bloque);
    nbloque += (idx-startMB)*BLOCKSIZE;

    if (idx > endMB) return error(OVERFLOW_BLOQUES);

    //Ini, busca, ajusta con bytes
    idx = -1;
    while (bloque[++idx] >= UCHAR_MAX);
    nbloque += idx;

    //Ini, busca, ajusta con bits
    unsigned char mascara = 128;
    unsigned char info    = bloque[idx];
    for(idx=0; mascara&info; idx++) mascara >>= 1;
    nbloque = nbloque*8+idx; //conversión de bytes a bits

    //Limpiamos el bloque
    memset(cmp, 0, BLOCKSIZE);
    bwrite(nbloque, cmp);

    //reservamos ese bloque
    escribir_bit(nbloque, RESERVAR);

    //printf(" ResBlo: %d\n", nbloque);
    return nbloque;
}


int liberar_bloque(int nbloque) {
    //printf(" LibBlo: %d\n", nbloque);
    return escribir_bit(nbloque, LIBERAR); }

int escribir_inodo(int ninodo, Inodo inodo)
{
    mi_waitSem();

    //Obtenemos un búffer (lo usaremos como SB y vector de inodos)
    unsigned char bloque[BLOCKSIZE];
    struct superbloque *SB = (struct superbloque*) bloque;
    p_Inodo AI = (p_Inodo) bloque;

    //encontramos su bloque absoluto y su índice en el bloque
    unsigned int  inodoAI;
    unsigned char desplInodo;

    //leemos el SB, para saber donde empieza el AI
    bread(BLOCK_SB, SB);
    inodoAI    = SB->posPrimerBloqueAI + (ninodo/(BLOCKSIZE/T_INODO));
    desplInodo = ninodo%(BLOCKSIZE/T_INODO);

    //leemos el bloque y escribimos la info
    bread(inodoAI, AI);
    AI[desplInodo].atime = time(NULL);
    AI[desplInodo].mtime = time(NULL);
    AI[desplInodo] = inodo;
    bwrite(inodoAI, AI);

    mi_signalSem();
    return 0;
}

Inodo leer_inodo(int ninodo)
{
    //Obtenemos un búffer (lo usaremos como SB y vector de inodos)
    unsigned char bloque[BLOCKSIZE];
    struct superbloque *SB = (struct superbloque*) bloque;
    p_Inodo AI = (p_Inodo) bloque;

    //encontramos su bloque absoluto y su índice en el bloque
    unsigned int  inodoAI;
    unsigned char desplInodo;

    //leemos el SB, para saber donde empieza el AI
    bread(BLOCK_SB, SB);
    inodoAI    = SB->posPrimerBloqueAI + (ninodo/(BLOCKSIZE/T_INODO));
    desplInodo = ninodo%(BLOCKSIZE/T_INODO);

    //leemos y escribimos la info
    mi_waitSem();
    bread(inodoAI, AI);
    AI[desplInodo].atime = time(NULL);
    bwrite(inodoAI, AI);
    mi_signalSem();

    return AI[desplInodo];
}


/**
 * Encuentra el primer inodo libre (lo tenemos en el SB)
 *  lo quita de la lista de libres y devuelve su número
 */
int reservar_inodo(unsigned char tipo, unsigned char permisos)
{
    mi_waitSem();

    //obtenemos dos búffers
    struct superbloque SB;
    Inodo inodo;

    //leemos el SB, nos quedamos con el valor de inodo libre
    bread(BLOCK_SB, &SB);
    int ninodo = SB.posPrimerInodoLibre/T_INODO;

    //si es válido modificamos el valor al siguiente y guardamos el SB
    if (ninodo > SB.totInodos) { mi_signalSem(); return error(OVERFLOW_INODOS); }

    //leemos el inodo
    inodo = leer_inodo(ninodo);

    //Ajusto los cambios en el SB
    SB.posPrimerInodoLibre = inodo.punteros[0];
    SB.cantInodosLibres--;
    bwrite(BLOCK_SB, &SB);

    //modifico los campos oportunos del inodo y guardo
    inodo.atime    = time(NULL);
    inodo.mtime    = time(NULL);
    inodo.ctime    = time(NULL);
    inodo.tipo     = tipo;
    inodo.permisos = permisos;
    inodo.punteros[0] = 0;
    escribir_inodo(ninodo, inodo);

    //devolvemos el ninodo
    mi_signalSem();
    return ninodo;
}

/**
 * Hacemos un recorrido de los datos del inodo liberando los punteros a bloque
 *  y luego liberando el bloque al que apuntaban
 */
int liberar_bloques_inodo(int ninodo, unsigned int bLogico, unsigned int bLogicoFin)
{
    Iterador_Inodo ruta;
    Iterador_Inodo rutaFin;

    int nFisico;
    obt_iin(bLogico, &ruta);
    obt_iin(bLogicoFin, &rutaFin); obt_fin_iin(&rutaFin);
    while ( es_iin_val_rango(&ruta, &rutaFin) ) {
	nFisico = cons_iin (ninodo, &ruta, OP_ELIMINACION);
	if (nFisico > 0) liberar_bloque(nFisico);
	sig_iin_arbol(&ruta);
	//print_iin(&ruta);
    }

    return 0;
}


int liberar_inodo(int ninodo)
{
    //Obtenemos un bloque y un inodo
    struct superbloque SB;
    Inodo inodo;

    mi_waitSem();
    //comprobamos que no estuviera ya libre
    inodo = leer_inodo(ninodo);
    if (inodo.tipo == LIBRE)  { mi_signalSem(); return error(VALOR_INCONSISTENTE); }
    unsigned int bLogicoFin = inodo.tamEnBytesLog / BLOCKSIZE;

    //lo marcamos libre y guardamos (aun no en la pila de libres)
    inodo.tipo = LIBRE;
    escribir_inodo(ninodo, inodo);
    mi_signalSem();

    //liberamos los bloques que estuvieran reservados
    liberar_bloques_inodo(ninodo, 0, bLogicoFin);

    mi_waitSem();
    //leemos el SB, ajustamos la cima de punterosLibres
    bread(BLOCK_SB, &SB);
    inodo = leer_inodo(ninodo);
    inodo.punteros[0] = SB.posPrimerInodoLibre;
    escribir_inodo(ninodo, inodo);

    //Ajusto los cambios en el SB
    SB.posPrimerInodoLibre = ninodo*T_INODO;
    SB.cantInodosLibres++;
    bwrite(BLOCK_SB, &SB);
    mi_signalSem();

    return 0;
}
