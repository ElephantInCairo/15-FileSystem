#include "iterador_inodo.h"

#define NULL_BLOQUE  0
#define NENTRADAS    (BLOCKSIZE/sizeof(unsigned int))
#define ENTRADA_MAX  (NENTRADAS -1)

//Operaciones aplicables a una ruta
int leer_valor_entrada    (int inodo, int nfisico, unsigned int *entrada, void *datos, char tipo);
int escribir_valor_entrada(int inodo, int nfisico, unsigned int *entrada, void *datos, char tipo);
int liberar_valor_entrada (int inodo, int nfisico, unsigned int *entrada, void *datos, char tipo);
char obt_altura(p_Iterador_Inodo it);


/**
 * Dado un número de bloque lógico,
 *  inicializa un Iterador_Inodo
 */
int obt_iin(unsigned int bLogico, p_Iterador_Inodo itIn)
{
    if (bLogico > FICHERO_MAX) return error(OVERFLOW_FICHERO);
    memset(itIn, 0, T_ITER_IN);

    //dada una altura del árbol devuelve su cantidad de bloques
    unsigned int cantidadBloques[4];
    cantidadBloques[0]=N_PTR_DIR;
    cantidadBloques[1]=NENTRADAS;

    //calculo de altura y posicinamiendo de bLogico al inicio de esa altura
    unsigned char idxAltura;
    for (idxAltura=0; bLogico>=cantidadBloques[idxAltura]; idxAltura++) {
        bLogico -= cantidadBloques[idxAltura];
        if (idxAltura > 0) cantidadBloques[idxAltura+1] = cantidadBloques[idxAltura]*NENTRADAS;
    }
    itIn->idxStack = idxAltura + 1;

    //ajustamos los campos que quedan
    char altura = idxAltura;
    if (altura == 0) itIn->v_entradas[0] = bLogico;
    else {
        //encontrar las entradas desde esa altura del árbol hasta el final
        unsigned char * entradas = itIn->v_entradas; //renames
        unsigned char idxEntradas = 0;
        for (; idxAltura>1; idxAltura--) {
            entradas[++idxEntradas] = bLogico/cantidadBloques[idxAltura-1];
            bLogico                -= (cantidadBloques[idxAltura-1]*entradas[idxAltura]);
        }
        entradas[++idxEntradas] = bLogico;
        entradas[0] = altura+PTR_DIR_MAX;
    }

    return 0;
}

char sig_iin_arbol(p_Iterador_Inodo itIn)
{
    //renames
    unsigned char * entrada  =  itIn->v_entradas;
    unsigned char * idxStack = &itIn->idxStack;
    unsigned char idxCim     = *idxStack - 1;

    char altura = obt_altura(itIn);

    //cálculo itIn
    if (entrada[idxCim] < ENTRADA_MAX) {
	//incrementa
        entrada[idxCim]++;
	//caso altura0 -> altura1
        if (idxCim == 0 && entrada[0] > PTR_DIR_MAX) altura++;
	//cuando limpiamos valores al empilar
        while (*idxStack <= altura) {
            idxCim++; entrada[idxCim]=0;
            (*idxStack)++;
        }
    //desempilar
    } else (*idxStack)--;

    return entrada[0] < N_PTR;
}

char sig_iin_datos(p_Iterador_Inodo itIn)
{
    //renames
    unsigned char * entrada  =  itIn->v_entradas;
    unsigned char * idxStack = &itIn->idxStack;
    unsigned char idxCim     = *idxStack - 1;

    //cálculo itIn
    if (entrada[idxCim] == ENTRADA_MAX) {
        do { entrada[idxCim]=0; idxCim--; }
        while (entrada[idxCim] == ENTRADA_MAX);
        if (idxCim == 0) entrada[(*idxStack)++]=0;
    }
    if (idxCim == 0 && entrada[0] == PTR_DIR_MAX) (*idxStack)++;
    entrada[idxCim]++;

    return entrada[0] < N_PTR;
}

char es_iin_val_rango(p_Iterador_Inodo iniRuta, p_Iterador_Inodo finRuta)
{
    //renames
    unsigned char *iniEntradas=iniRuta->v_entradas;
    unsigned char *finEntradas=finRuta->v_entradas;

    unsigned char idxAltura = 0;
    unsigned char end       = finRuta->idxStack;
    while( idxAltura<end && iniEntradas[idxAltura] == finEntradas[idxAltura]) idxAltura++;
    return iniEntradas[idxAltura] < finEntradas[idxAltura];
}

char es_iin_val(p_Iterador_Inodo iniRuta) { return iniRuta->v_entradas[0]<N_PTR; }

char tipo_ruta(p_Iterador_Inodo itIn)
{
    char altura = obt_altura(itIn);
    if (altura == itIn->idxStack-1) return HOJAS;
    else                            return ARBOL;
}

int _cons_iin(int ninodo, p_Iterador_Inodo itIn, char operacion)
{
    //Obtenemos un bloque (guardaremos el inodo o un bloque de Inodos)
    unsigned int bloque[NENTRADAS];
    p_Inodo inodo = (p_Inodo) bloque;
    *inodo = leer_inodo(ninodo); //leeremos el inodo usando el bloque

    //tratar: operación a realizar sobre el bloque
    p_func tratar;
    if (operacion == OP_ESCRITURA) tratar = escribir_valor_entrada;
    else                           tratar = leer_valor_entrada;

    //último campo a seguir (de itIn) + tratar: eliminación
    char end = (itIn->idxStack)-1;
    if (operacion == OP_ELIMINACION && end==0)
        tratar = liberar_valor_entrada;

    //p_entrada: puntero a la entrada que estamos leyendo
    unsigned int * p_entrada;
    p_entrada = inodo->punteros + itIn->v_entradas[0];

    //indica si la ruta es ARBOL o HOJA
    char t_ruta = tipo_ruta(itIn);

    //tratamos el primer campo (que va a ser el del inodo)
    int nFisico;
    if (end>=0) nFisico = tratar(ninodo, ninodo, p_entrada, inodo, INODO|t_ruta);
    if (nFisico == NULL_BLOQUE) return NULL_BLOQUE_W;

    unsigned int  entrada;
    unsigned char idxRuta;
    for (idxRuta=1; idxRuta<=end; idxRuta++) {
        entrada   = itIn->v_entradas[idxRuta];
        p_entrada = bloque+entrada;
        if (operacion == OP_ELIMINACION && idxRuta==end)
            tratar = liberar_valor_entrada;
        //tratamos los siguientes campos (que van a ser bloquesInodo)
        nFisico = tratar(ninodo, nFisico, p_entrada, bloque, BLOQUE|t_ruta);
	if (nFisico == NULL_BLOQUE) return NULL_BLOQUE_W;
    }

    return nFisico;
}

int cons_iin(int ninodo, p_Iterador_Inodo itIn, char operacion)
{
    int status;
    mi_waitSem();
    status = _cons_iin(ninodo, itIn, operacion);
    mi_signalSem();
    return status;
}

void print_iin(p_Iterador_Inodo itIn)
{
    unsigned char idxAltura;
    for(idxAltura=0; idxAltura < itIn->idxStack; idxAltura++)
        printf(" itIn; p%u: %d\n", idxAltura, itIn->v_entradas[idxAltura]);
    printf(" itIn; iS: %d\n", itIn->idxStack);
}


/**
 * Lee y devuelve en nbloque que tenemos en *p_entrada
 */
int leer_valor_entrada(int ninodo, int nfisico, unsigned int * p_entrada, void * p_datos, char tipo)
{
    //printf(" LVE valor de la entrada: %u\n", *p_entrada);

    //guardamos el valor del bloque leído actual
    unsigned int nbloque = *p_entrada;

    //sobreescribimos para leer el próximo
    if (nbloque == 0) memset(p_datos, 0, BLOCKSIZE);
    else              bread(nbloque, p_datos);

    return nbloque;
}

/**
 * Lee el bloque que hay en en campo ptr.
 * Si el campo estaba vacío, reserva un bloque
 *  y devuelve ese bloque
 */
int escribir_valor_entrada(int ninodo, int nfisico, unsigned int * p_entrada, void * p_datos, char tipo)
{
    //printf(" EVE valor de la entrada: %u\n", *p_entrada);

    //si el puntero es nulo hay que reservar bloque y actualizar el valor de la entrada
    if (*p_entrada == NULL_BLOQUE) {
        *p_entrada = reservar_bloque();
        if ((tipo & INODO) == INODO) escribir_inodo(nfisico, *((p_Inodo) p_datos));
        else                         bwrite        (nfisico, p_datos);
	Inodo inodo = leer_inodo(ninodo);
	inodo.numBloquesOcupados++;
	escribir_inodo(ninodo, inodo);
    }

    //guardamos el valor del bloque leído actual
    unsigned int nbloque = *p_entrada;


    //obtenemos el bloque
    bread(*p_entrada, p_datos);

    return nbloque;
}

 /**
  * En esta función no se devuelve el bloque
  * */
int liberar_valor_entrada(int ninodo, int nfisico, unsigned int * p_entrada, void * p_datos, char tipo)
{
    //printf(" XVE valor de la entrada: %u\n", *p_entrada);

    //guardamos el valor del bloque leído actual
    unsigned int nbloque = *p_entrada;

    unsigned char bloqueVacio[BLOCKSIZE];
    if ((tipo & ARBOL) == ARBOL) {
	//leemos y comparamos
	bread(*p_entrada, p_datos);
	memset(bloqueVacio, 0, BLOCKSIZE);
	if (memcmp(p_datos, bloqueVacio, BLOCKSIZE)>0) return 0;
    }

    //obtenemos el bloque
    *p_entrada = NULL_BLOQUE;
    if (tipo & INODO) escribir_inodo(nfisico, *((p_Inodo) p_datos));
    else                      bwrite(nfisico, p_datos);
    Inodo inodo = leer_inodo(ninodo);
    inodo.numBloquesOcupados--;
    escribir_inodo(ninodo, inodo);

    return nbloque;
}

char obt_altura(p_Iterador_Inodo it) { return (it->v_entradas[0] >= N_PTR_DIR) ? it->v_entradas[0]-PTR_DIR_MAX : 0; }

void obt_fin_iin(p_Iterador_Inodo it) { it->v_entradas[0]++; }
