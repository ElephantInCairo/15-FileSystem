#include "directorios.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/**
 * Primer argumento  (1): nombre del SF
 * Segundo argumento (2): ruta_original
 * Tercer argumento  (3): ruta_enlace
 */
int main(int argc, char *argv[]) {

    char * nombre_sf;
    char * ruta_original;
    char * ruta_enlace;

    //confirmar que los argumentos estan bien
    if(argc != 4) {
        printf("Formato: %s nombre_sf /ruta_original /ruta_enlace\n", argv[0]);
        return error(NUMERO_PARAMETROS);
    }

    //ponemos nombres legibles
    nombre_sf      = argv[1];
    ruta_original  = argv[2];
    ruta_enlace    = argv[3];

    //montamos el sistema de ficheros
    bmount(nombre_sf);

    mi_link(ruta_original, ruta_enlace);

    //desmontamos el sistema de ficheros
    bumount();

    return 0;
}
