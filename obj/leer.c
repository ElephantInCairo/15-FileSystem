#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "ficheros.h"

#define TAM_BUFFER 1500

/**
 * Primer argumento  (1): nombre del SF
 * Segundo argumento (2): número del inodo
 */
int main(int argc, char *argv[]) {

    //variables
    Inodo inodo;
    unsigned int ninodo;
    unsigned int offset;
    unsigned int bytesLeidos;
    unsigned char buffer[TAM_BUFFER];
    char mensaje[128];
    char * nombre_sf;

    //confirmar que los argumentos están bien
    if(argc != 3) {
        printf("Formato: %s nombre_sf ninodo\n", argv[0]);
        return -1;
    }

    //damos un nombre legible a los argumentos
    nombre_sf = argv[1];
    ninodo    = atoi(argv[2]);
    memset(buffer, 0, TAM_BUFFER);

    //montamos el sistema de ficheros
    bmount(nombre_sf);

    //hacemos un recorrido secuencial del inodo
    //si bytesLeidos = TAM_BUFFER ese tamaño estaba lleno y hay que seguir
    //si bytesLeidos < TAM_BUFFER hemos llegado al fin del fichero (tamEnBytesLog)
    //si bytesLeidos = -1 ese bloque estaba vacío pero hay que seguir leyendo
    offset = 0;
    do {
        bytesLeidos = mi_read_f(ninodo, (void*) buffer, offset, TAM_BUFFER);
        offset     += bytesLeidos;
        write(1, buffer, bytesLeidos);
    } while (bytesLeidos==TAM_BUFFER || bytesLeidos==-1);

    //mostramos la información de la lectura
    sprintf(mensaje, "\nbytes leídos: %u\n", offset);
    write(2, mensaje, strlen(mensaje));

    //mostramos la información del inodo
    inodo = leer_inodo(ninodo);
    sprintf(mensaje, "tamaño en bytes lógicos: %u\n", inodo.tamEnBytesLog);
    write(2, mensaje, strlen(mensaje));

    //desmontamos el sistema de ficheros
    bumount();

    return 0;
}
