#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "directorios.h"

#define STR(x)   #x
#define SHOW_DEFINE(x) printf("%s=%s\n", #x, STR(x))

int main(int argc, char **argv)
{
    int opc;
    int num;
    int ninodo;
    unsigned int aux;
    Inodo inod0;

    Iterador_Inodo ruta;
    Iterador_Inodo fin_ruta;

    char nombre_dir [60];
    char buffer[BLOCKSIZE*4];
    unsigned int  buffInodos[BLOCKSIZE/sizeof(unsigned int)];
    const char * path_sf  = (const char *) buffer;
    const char * buffer_f = (const char *) buffer;
    //p_Inodo AI = (p_Inodo) buffer;

    printf("Introduce el nombre del SF: ");
    scanf("%s", buffer);

    bmount(path_sf);

    unsigned char buffSB[BLOCKSIZE];
    struct superbloque *SB = (struct superbloque*) buffSB;
    bread(BLOCK_SB, SB);

    do {
        printf("[1] Leer ruta\n");
        printf("[2] Escribir ruta\n");
        printf("[3] Eliminar ruta\n");
        printf("[4] Obtener ruta\n");
        printf("[5] Siguiente ruta HOJA\n");
        printf("[7] Escribir nuevo buffer\n");
        printf("[8] Leer bloque en bruto\n");
        printf("[9] Leer bInodos en bruto\n");
        printf("[11] Info inodo\n");
        printf("[13] Reservar bloque\n");
        printf("[14] Liberar bloque\n");
        printf("[15] Reservar inodo\n");
        printf("[16] Liberar inodo\n");
        printf("[17] Leer bit\n");
        printf("[18] Escribir bit\n");
        printf("[22] Separador de ruta\n");
        printf("[33] Obtener inodo\n");
        printf("[34] Siguiente ruta ARBOL\n");
        printf("[88] Formatear SF\n");
        printf("[95] InitMB + tamMB\n");
        printf("[96] InitAI + tamAI\n");
        printf("[97] Hasta bLogico X FULLA\n");
        printf("[98] Hasta bLogico X ARBOL\n");
        printf("[99] Mostrar SB\n");
        printf("[0] Salir\n");
        printf("Introduce una opción: ");
        scanf("%d", &opc);
        printf("\n");
        switch(opc)
        {
	    case 0:
		printf("It's been a pleasure!");
		break;
	    case 1: //Leer ruta
		printf("Introduce ninodo: ");
		scanf("%u", &aux);
		aux = cons_iin(aux, &ruta, OP_LECTURA);
		liberar_bloque(aux);
		printf("el bfisico es: %u\n", aux);
		break;
	    case 2: //Escribir ruta
		printf("Introduce ninodo: ");
		scanf("%u", &aux);
		aux = cons_iin(aux, &ruta, OP_ESCRITURA);
		printf("el bfisico es: %u\n", aux);
		break;
	    case 3: //Eliminar cima ruta
		printf("Introduce ninodo: ");
		scanf("%u", &aux);
		aux = cons_iin(aux, &ruta, OP_ELIMINACION);
		printf("el bfisico es: %u\n", aux);
		break;
	    case 4: //Obtener ruta
		printf("Introduce nbloque: ");
		scanf("%d", &num);
		obt_iin(num, &ruta);
		print_iin(&ruta);
		break;
	    case 5: //sig_iin_datos
		sig_iin_datos(&ruta);
		print_iin(&ruta);
		break;
	    case 7: //escribir buffer reservando inodo
		printf("offset: "); scanf("%u", &aux); getchar();
		printf("Texto: "); scanf("%s", buffer); //getchar();
		num = reservar_inodo(FICHERO, 6);
		printf("ninodo: %u\n", num);
		printf("tamaño entrada: %u\n", (unsigned) strlen(buffer_f));
		mi_write_f((unsigned) num, buffer_f, aux, (unsigned) strlen(buffer_f) );
		break;
	    case 8: //Leer bloque en bruto
		printf("Introduce nbloque: ");
		scanf("%d", &num);
		bread(num, buffer);
		write(2, buffer, BLOCKSIZE); printf("\n");
		break;
	    case 9: //Leer bloqueInodos en bruto
		printf("Introduce nbloque: ");
		scanf("%d", &num);
		bread(num, buffInodos);
		for (aux=0; aux<256; aux++) {
		    if (buffInodos[aux]>0)
			printf("p: %u, bl: %u\n", aux, buffInodos[aux]);
		}
		break;
	    case 11:
		printf("leer inodo: "); scanf("%u", &num); getchar();
		char atime[80], mtime[80], ctime[80];
		while (num>=0) {
		    inod0 = leer_inodo(num);
		    printf("Info del inodo %u\n", num);
		    for (aux=0; aux<N_PTR; aux++) printf("puntDir[%d]: %d\n", aux, inod0.punteros[aux]);
		    strftime(atime, sizeof(atime), "%a %Y­%m­%d %H:%M:%S", localtime(&inod0.atime));
		    strftime(mtime, sizeof(mtime), "%a %Y­%m­%d %H:%M:%S", localtime(&inod0.mtime));
		    strftime(ctime, sizeof(ctime), "%a %Y­%m­%d %H:%M:%S", localtime(&inod0.ctime));
		    printf("ATIME: %s\n", atime);
		    printf("MTIME: %s\n", mtime);
		    printf("CTIME: %s\n", ctime);
		    printf("leer inodo: "); scanf("%u", &num); getchar();
		}
		break;
	    case 13: //Reservar bloque
		printf("Reservando bloque...\n");
		printf("El bloque reservado es el %d\n", reservar_bloque() );
		break;
	    case 14: //liberar bloque
		printf("Introduce nbloque: ");
		scanf("%d", &num);
		while (num != 0) {
		    if (liberar_bloque(num))
			printf("El bloque ha sido liberado\n");
		    printf("Introduce nbloque: ");
		    scanf("%d", &num);
		}
		break;
	    case 15:
		printf("ninodo: %d\n", reservar_inodo(FICHERO, 6));
		break;
	    case 16:
		printf("Introduce ninodo: ");
		scanf("%u", &aux);
		liberar_inodo(aux);
		break;
	    case 17:
		printf("Introduce nbloque: ");
		scanf("%u", &aux);
		while (aux > 0) {
		    printf("valor: %d\n", leer_bit(aux) );
		    printf("Introduce nbloque: ");
		    scanf("%u", &aux);
		}
		break;
	    case 18:
		printf("Introduce nbloque: ");
		scanf("%d", &num);
		while (num != 0) {
		    if (num > 0) escribir_bit( num, 1);
		    else         escribir_bit(-num, 0);
		    printf("Introduce nbloque: ");
		    scanf("%d", &num);
		}
		break;
	    case 22:
		printf("Camino: "); scanf("%s", buffer); //getchar();
		/*
		aux = entradas_a_recorrer(buffer);
		p_nombre_final = buffer;
		for (num=0; num<aux; num++) {
		    extraer_camino (p_nombre_final, nombre_dir, &p_nombre_final);
		    printf("nombre: %s\n", nombre_dir);
		    printf("final : %s\n", buffer);
		    //buffer = nombre_final;
		}
		*/
		char t_inodo;
		Iterador_Camino it;
		primero(buffer, &it);
		while ( es_cam_val(buffer, &it) ) {
		    cons_and_sig(buffer, &it, nombre_dir, &t_inodo);
		    printf("nombre: %s\n", nombre_dir);
		}
		break;
	    case 33:
		printf("Camino: "); scanf("%s", buffer); //getchar();
		ninodo = 0;
		obtener_inodo(buffer, &ninodo, MIXTO);
		break;
	    case 198:
		//test correctesa write
		printf("Introduce nbloque: ");
		scanf("%d", &num);
		memset(buffer, 0, BLOCKSIZE);
		buffer[0] = 64;
		bwrite(num, buffer);
		break;
	    case 34: //Siguiente ruta ARBOL
		sig_iin_arbol(&ruta);
		print_iin(&ruta);
		break;
	    case 88:
		printf("Introduce nbloques: ");
		scanf("%u", &aux);
		initSB(aux, aux/4);
		initMB(aux);
		initAI(aux/4);
		reservar_inodo(DIRECTORIO, 6);
		break;
	    case 95: //bloques
		bread(BLOCK_SB, SB);
		printf("El SF tiene %d bloques\n", (*SB).totBloques);
		initMB(aux);
		printf("El MB ocupa %d bloques\n", tamMB((*SB).totBloques) );
		printf("El cálculo es (nbloques/8)/BLOCKSIZE (más uno si es preciso)\n");
		break;
	    case 96: //inodos
		bread(BLOCK_SB, SB);
		printf("El SF tiene %d inodos\n", (*SB).totInodos);
		initMB(aux);
		printf("El AI ocupa %d bloques\n", tamAI((*SB).totInodos) );
		printf("El cálculo es ninodos/(BLOCKSIZE/T_INODO) (más uno si es preciso)\n");
		break;
	    case 97: //recorrer ruta hasta X HOJAS
		printf("Introduce bLogico: ");
		scanf("%u", &aux);
		obt_iin(0,   &ruta);
		obt_iin(aux, &fin_ruta);
		aux = 0;
		while( es_iin_val_rango(&ruta, &fin_ruta) ) {
		    sig_iin_datos(&ruta);
		    //print_iin(&ruta); printf("\n");
		    aux++;
		}
		printf("Se han hecho %d iteraciones\n", aux);
		print_iin(&ruta);
		//for(num=0; num<aux; num++) siguiente(&ruta, HOJAS);
		break;
	    case 98: //recorrer ruta hasta X ARBOL
		printf("Introduce bLogico: ");
		scanf("%u", &aux);
		obt_iin(0,   &ruta);
		obt_iin(aux, &fin_ruta);
		aux = 0;
		while( es_iin_val_rango(&ruta, &fin_ruta) ) {
		    sig_iin_arbol(&ruta);
		    //print_iin(&ruta); printf("\n");
		    aux++;
		}
		printf("Se han hecho %d iteraciones\n", aux);
		print_iin(&ruta);
		//for(num=0; num<aux; num++) siguiente(&ruta, HOJAS);
		break;
	    case 99: //mostrar SB
		bread(BLOCK_SB, SB);
		SHOW_DEFINE(BLOCK_SB);
		printf("=========SB========\n");
		printf("firstMB:\t%u\n", (*SB).posPrimerBloqueMB);
		printf("lastMB: \t%u\n",  (*SB).posUltimoBloqueMB);
		printf("fAI:    \t%u\n",  (*SB).posPrimerBloqueAI);
		printf("lAI:    \t%u\n",  (*SB).posUltimoBloqueAI);
		printf("fData:  \t%u\n",  (*SB).posPrimerBloqueDatos);
		printf("lData:  \t%u\n",  (*SB).posUltimoBloqueDatos);
		printf("pos '/':\t%u\n", (*SB).posInodoRaiz);
		printf("pos 'l':\t%u\n", (*SB).posPrimerInodoLibre);
		printf("#bloqLibres:\t%u\n", (*SB).cantBloquesLibres);
		printf("#inodLibres:\t%u\n", (*SB).cantInodosLibres);
		printf("#bloq:  \t%u\n", (*SB).totBloques);
		printf("#inod:  \t%u\n", (*SB).totInodos);
		break;
	    default:
		printf("Mec! ERROR, try again\n");
		break;
	    /*
	    case 9:
		printf("Introduce nblogico: "); scanf("%d", &aux);
		while (aux != 0) {
		    num = traducir_bloque_inodo(1, aux, OP_ESCRITURA);
		    printf("bloque f obtenido: %d\n", num); printf("\n");
		    printf("Introduce nblogico: "); scanf("%d", &aux);
		}
		break;
	    */
        }
    } while (opc != 0);
    printf("\n");
    bumount();
    return 0;
}
