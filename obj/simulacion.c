#include <pthread.h>
#include "simulacion.h"

#define T_SCAMINO    50

//var.
char camino[T_SCAMINO];

//func.
void * sim_write(void * ptr)
{
    unsigned int my_pid = *((unsigned int *) ptr);
    char fichero1[256];
    char directorio1[256];
    char * p_fichero    = fichero1;
    char * p_directorio = directorio1;

    //crear el directorio
    snprintf(p_directorio, 256, "%sprocesos_%d/", camino, my_pid);
    mi_create(directorio1, 7, DIRECTORIO);

    //crear el fichero
    snprintf(p_fichero, 256, "%sprueba.dat", directorio1);
    mi_create(fichero1, 7, FICHERO);

    //50 escrituras
    int idxOp;
    Registro reg;
    unsigned int pos     = 1;
    unsigned int semilla = (unsigned int) time(NULL);
    for (idxOp=1; idxOp<=NUM_ESCR; idxOp++) {
	pos = (semilla*pos*idxOp*my_pid) % (POS_MAX/T_REGISTRO);

	reg.fecha      = time(NULL);
	reg.pos        = pos;
	reg.nescritura = idxOp;
	reg.pid        = my_pid;

	printf("\tEscritura número %d\n", pos*T_REGISTRO);
	mi_write(fichero1, &reg, pos*T_REGISTRO, T_REGISTRO);
	usleep(500);
    }

    return ptr;
}

int main(int argc, char *argv[])
{
    char * sistema_ficheros;

    if (argc != 2) {
        printf("Formato: %s nombre_sf", argv[0]);
        return -1;
    }

    //ponemos nombres legibles
    sistema_ficheros = argv[1];

    //montamos el sistema
    bmount(sistema_ficheros);

    //var. concurr
    pthread_t     v_thr[NUM_THREADS];
    unsigned int  v_pid[NUM_THREADS];

    //var.
    time_t ahora          = time(NULL);
    struct tm * info_time = localtime(&ahora);

    //creamos el directorio
    char * p_camino = camino;
    strftime(p_camino, T_SCAMINO, "/simul_%Y%m%d%H%M%S/", info_time);
    mi_create(camino, 7, DIRECTORIO);

    //iniciamos NUM_THREADS hilos
    int idxThr;
    for (idxThr=0; idxThr<NUM_THREADS; idxThr++) {
        v_pid[idxThr] = idxThr;
        pthread_create(&v_thr[idxThr], NULL, sim_write, (void *) &v_pid[idxThr]);
	usleep(20000);
    }

    //esperamos a que terminen
    for (idxThr=0; idxThr<NUM_THREADS; idxThr++) {
        pthread_join(v_thr[idxThr], NULL);
    }

    //desmontamos el sistema
    bumount(sistema_ficheros);

    return 0;
}

