#include "directorios.h"

#define NUM_THREADS  100
#define NUM_ESCR      50
#define POS_MAX   500000

#define T_REGISTRO ((int) sizeof(struct registro_s))

struct registro_s
{
    time_t fecha;
    unsigned int pid;
    unsigned int nescritura;
    unsigned int pos;
};


typedef struct registro_s Registro;
typedef struct registro_s * p_Registro;
