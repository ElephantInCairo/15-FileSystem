#include "g_errores.h"
#include "semaforo_mutex_posix.h"


#define BLOCKSIZE 1024              /* múltiple de inodo y char */
#define PERM_HD   O_RDWR|O_CREAT
#define MODE_HD   0666


int bmount(const char *path);
int bumount();
int bwrite(unsigned int nblock, const void *buf);
int bread (unsigned int nblock,       void *buf);

#define mi_waitSem()   waitSem()
#define mi_signalSem() signalSem()
