#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "ficheros.h"

/**
 * Primer argumento  (1): nombre del SF
 * Segundo argumento (2): buffer a almacenar
 */
int main(int argc, char *argv[]) {

    //variables
    unsigned int lenInput  = (unsigned) strlen(argv[3]);
    unsigned int ninodo;
    unsigned int bLogico;
    unsigned char bufOut[lenInput];
    char * bufIn;
    char * nombre_sf;
    int tam;

    //confirmar que los argumentos estan bien
    if(argc != 4) {
        printf("Formato: %s nombre_sf bloque_logico buffer\n", argv[0]);
        return -1;
    }

    //ponemos nombres legibles
    nombre_sf = argv[1];
    bLogico   = atoi(argv[2]);
    bufIn     = argv[3];
    memset(bufOut, 0, lenInput);

    //montamos el sistema de ficheros
    bmount(nombre_sf);

    //reservamos un inodo
    ninodo = reservar_inodo(FICHERO, 6);
    if (ninodo == -1) {
        printf("No queda espacio para más ficheros");
        return -1;
    }

    //escribimos
    mi_write_f(ninodo, bufIn, bLogico, lenInput);

    //leemos obteniendo los bytes leídos
    tam = mi_read_f(ninodo, bufOut, bLogico, lenInput);

    //mostramos la info
    printf("Inodo reservado: %u\n", ninodo);
    printf("El tamaño leido es %d\n", tam);
    printf("Búffer leído del fichero: \n");
    write(2, bufOut, tam); printf("\n");
    printf("\n");

    //desmontamos el sistema de ficheros
    bumount();

    return 0;
}
