#include "ficheros_basico.h"

struct stat_s {
	time_t atime; //8bytes
        time_t mtime;
        time_t ctime;

        unsigned int nlinks; //4bytes
        unsigned int tamEnBytesLog;
        unsigned int numBloquesOcupados;

        unsigned char tipo; //1byte
        unsigned char permisos;
};

//stat
typedef struct stat_s Stat;
typedef struct stat_s * p_Stat;

int mi_write_f   (unsigned int ninodo, const void *buf_original, unsigned int offset, unsigned int nbytes);
int mi_read_f    (unsigned int ninodo,       void *buf_original, unsigned int offset, unsigned int nbytes);
int mi_chmod_f   (unsigned int ninodo, unsigned char modo);
int mi_truncar_f (unsigned int ninodo, unsigned int nbytes);
int mi_stat_f    (unsigned int ninodo, p_Stat p_stat);
int mi_link_f    (unsigned int ninodo);
int mi_unlink_f  (unsigned int ninodo);
