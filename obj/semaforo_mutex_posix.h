#include <pthread.h>
#include <semaphore.h>
#include <stdlib.h>

/* name debe ser un nombre de caracteres ascii que comienze con "/", p.e. "/mimutex" */
#define SEM_NAME "/reentrant"  /* Usamos este nombre para el semáforo mutex reentrante*/
#define SEM_COND "/cond_mutex" /* Usamos este nombre para el semaforo mutex de condición*/
#define SEM_INIT_VALUE 1       /* Valor inicial de los mutex */

void initSem();
void waitSem();
void deleteSem();
void signalSem();
