#include "ficheros.h"

typedef unsigned int Iterador_Camino;
typedef unsigned int * p_Iterador_Camino;

char es_dir_padre_valido (const char * camino, p_Iterador_Camino pit);
char   es_cam_val (const char * camino, p_Iterador_Camino pit);
void      primero (const char * camino, p_Iterador_Camino pit);
void cons_and_sig (const char * camino, p_Iterador_Camino pit, char * inicial, char * tipo);
