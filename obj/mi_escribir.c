#include "directorios.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/**
 * Primer argumento  (1): nombre del SF
 * Segundo argumento (2): ruta
 * Tercer argumento  (3): texto
 */
int main(int argc, char *argv[]) {

    char * nombre_sf;
    char * ruta;
    char * buff;

    //confirmar que los argumentos estan bien
    if(argc != 4) {
        printf("Formato: %s nombre_sf /ruta_fichero texto\n", argv[0]);
        return error(NUMERO_PARAMETROS);
    }

    //ponemos nombres legibles
    nombre_sf = argv[1];
    ruta      = argv[2];
    buff      = argv[3];

    //montamos el sistema de ficheros
    bmount(nombre_sf);

    mi_write(ruta, buff, 0, strlen(buff));

    //desmontamos el sistema de ficheros
    bumount();

    return 0;
}
