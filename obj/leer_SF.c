#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "ficheros.h"

#define STR(x)   #x
#define SHOW_DEFINE(x) printf("%s=%s\n", #x, STR(x))
#define T_STIME 60

int main(int argc, char **argv)
{
    int opc;
    int num;
    unsigned int ini;
    unsigned int fin;
    unsigned int nbloque;
    unsigned int ninodo;
    char atime[T_STIME];
    char mtime[T_STIME];
    char ctime[T_STIME];
    char * p_atime = atime;
    char * p_mtime = mtime;
    char * p_ctime = ctime;
    Inodo inod0;

    //buffers
    unsigned char buffer[BLOCKSIZE*4];
    unsigned char buffSB[BLOCKSIZE];
    const char *    path_sf = (const char *)         buffer;
    struct superbloque * SB = (struct superbloque *) buffSB;

    //leemos el SF
    printf("Introduce el nombre del SF: ");
    scanf("%s", buffer); bmount(path_sf);

    do {
	printf("\n");
	printf("[1] Mostrar todos los campos del SB\n");
	printf("[2] Mostrar el tamaño en bloques del MB y AI\n");
        printf("[3] Mostrar el tamaño de la estructura Inodo\n");
	printf("[4] Mostrar info de los inodos no libres\n");
	printf("[5] Mostrar bloques reservados del MB\n");
	printf("[0] Salir\n");
	printf("Introduce una opción: "); scanf("%d", &opc);
	printf("\n");

	switch(opc)
	{
	    case 1:
		bread(BLOCK_SB, SB);
		//SHOW_DEFINE(BLOCK_SB);
		printf("=========SB========\n");
		printf("primerMB:   \t%u\n", (*SB).posPrimerBloqueMB);
		printf("últimoMB:   \t%u\n", (*SB).posUltimoBloqueMB);
		printf("primerAI:   \t%u\n", (*SB).posPrimerBloqueAI);
		printf("últimoAI:   \t%u\n", (*SB).posUltimoBloqueAI);
		printf("primerData: \t%u\n", (*SB).posPrimerBloqueDatos);
		printf("últimoData: \t%u\n", (*SB).posUltimoBloqueDatos);
		printf("pos '/':    \t%u\n", (*SB).posInodoRaiz);
		printf("pos 'l':    \t%u\n", (*SB).posPrimerInodoLibre);
		printf("#bloqLibres:\t%u\n", (*SB).cantBloquesLibres);
		printf("#inodLibres:\t%u\n", (*SB).cantInodosLibres);
		printf("#bloq:      \t%u\n", (*SB).totBloques);
		printf("#inod:      \t%u\n", (*SB).totInodos);
		break;
	    case 2:
		bread(BLOCK_SB, SB);
		printf("Tamaño del MB: %d\n", tamMB((*SB).totBloques));
		printf("Tamaño del AI: %d\n", tamAI((*SB).totInodos));
		break;
	    case 3:
		printf("Tamaño estructura inodo: %lu\n", sizeof(Inodo));
		break;
	    case 4:
		bread(BLOCK_SB, SB);
		ini = 0;
		fin = (*SB).totInodos;
		for (ninodo=ini; ninodo<fin; ninodo++) {
		    inod0 = leer_inodo(ninodo);
		    if (inod0.tipo == LIBRE) continue;
		    printf("Info del inodo %u\n", ninodo);
		    printf("Tipo de inodo: %c\n", inod0.tipo);
		    printf("Permisos: %d\n", (int) inod0.permisos);
		    printf("Nlinks:   %d\n", inod0.nlinks);
		    printf("bloques ocupados: %u\n", inod0.numBloquesOcupados);
		    printf("bytes lógicos:    %u\n", inod0.tamEnBytesLog);
		    for (num=0; num<N_PTR; num++) printf("puntDir[%d]: %d\n", num, inod0.punteros[num]);
		    strftime(p_atime, T_STIME, "%a %Y­%m­%d %H:%M:%S", localtime(&inod0.atime));
		    strftime(p_mtime, T_STIME, "%a %Y­%m­%d %H:%M:%S", localtime(&inod0.mtime));
		    strftime(p_ctime, T_STIME, "%a %Y­%m­%d %H:%M:%S", localtime(&inod0.ctime));
		    printf("ATIME: %s\n", p_atime);
		    printf("MTIME: %s\n", p_mtime);
		    printf("CTIME: %s\n", p_ctime);
		    printf("\n");
		}

		break;
	    case 5:
		bread(BLOCK_SB, SB);
		ini = 0; fin = (*SB).totBloques;
		printf("Bloques ocupados: ");
		for (nbloque=ini; nbloque<fin; nbloque++)
		    if (leer_bit(nbloque) == 1) printf("%d, ", nbloque);
		printf("\n");
		break;
	    case 0:
		printf("Fin de la ejecución\n");
		break;
	    default:
		printf("Mec! Error, intenta de nuevo\n");
		break;
	}
	printf("\n");
    } while (opc != 0);
    printf("\n");

    bumount();
    return 0;
}
