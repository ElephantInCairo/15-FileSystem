#include <fcntl.h>
#include <stdio.h>
#include <sys/stat.h>
#include <semaphore.h>

#include "semaforo_mutex_posix.h"

unsigned int deepth;
unsigned int thr_waiting;
pthread_t tid;
sem_t * reentrant_lock;
sem_t * cond_mutex;

/**
 * Semáforo reentrante.
 *  -deepth: profundidad del hilo actual.
 *
 *  -thr_waiting: número de threads que están usando el semáforo
 *
 *  -tid: hilo que está usando el semáforo (o el último que lo hizo)
 *   no necesita inicializar ya que tenemos el número de threads que
 *   están usando el semáforo
 */

void initSem()
{
    deepth = 0; thr_waiting = 0;

    //mutex reentrante
    reentrant_lock = sem_open(SEM_NAME, O_CREAT, S_IRWXU, SEM_INIT_VALUE);

    //mutex de condición
    cond_mutex     = sem_open(SEM_COND, O_CREAT, S_IRWXU, 1);
}

void deleteSem()
{
    deepth = 0; thr_waiting = 0;
    sem_unlink(SEM_NAME);
    sem_unlink(SEM_COND);
}

/**
 * Ajustamos tid al actual cuando es el primer hilo
 *  que entra en el semáforo para evitar falsos positivos
 *  por basura en tid en los siguientes threads que entran.
 */
void waitSem()
{
    pthread_t this_tid = pthread_self();

    sem_wait(cond_mutex);
    char is_owner = (deepth && pthread_equal(tid, this_tid));
    if (is_owner) deepth++; else thr_waiting++;
    sem_post(cond_mutex);

    if (!is_owner) {
        sem_wait(reentrant_lock);
        tid = this_tid;
        deepth++;
    }

    //printf("W: thr_waiting: %d, deepth: %d\n", thr_waiting, deepth);
}

void signalSem()
{
    pthread_t this_tid = pthread_self();

    sem_wait(cond_mutex);
    if ( pthread_equal(tid, this_tid) ) {
	deepth--;
	if (deepth == 0) {
	    thr_waiting--;
	    sem_post(reentrant_lock);
	}
    }
    //printf("S: thr_waiting: %d, deepth: %d\n", thr_waiting, deepth);
    sem_post(cond_mutex);
}
