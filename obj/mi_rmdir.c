#include "directorios.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/**
 * Primer argumento  (1): nombre del SF
 * Segundo argumento (2): ruta
 */
int main(int argc, char *argv[]) {

    char * nombre_sf;
    char * ruta;

    //confirmar que los argumentos estan bien
    if(argc != 3) {
        printf("Formato: %s nombre_sf /ruta\n", argv[0]);
        return error(NUMERO_PARAMETROS);
    }

    //ponemos nombres legibles
    nombre_sf = argv[1];
    ruta      = argv[2];

    //montamos el sistema de ficheros
    bmount(nombre_sf);

    mi_unlink(ruta, DIRECTORIO);

    //desmontamos el sistema de ficheros
    bumount();

    return 0;
}
