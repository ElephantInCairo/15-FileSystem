#include <pthread.h>
#include "simulacion.h"

void * processo_hijo();

int main(int argc, char *argv[])
{
    char * sistema_ficheros;

    if (argc != 2) {
        printf("Formato: %s nombre_sf", argv[0]);
        return -1;
    }

    //ponemos nombres legibles
    sistema_ficheros = argv[1];

    //montamos el sistema
    bmount(sistema_ficheros);

    //var. concurr
    pthread_t v_thr[NUM_THREADS];
    Registro  v_reg[NUM_THREADS];

    //iniciamos NUM_THREADS hilos
    int idxThr;
    for (idxThr=0; idxThr<NUM_THREADS; idxThr++) {
        v_reg[idxThr].pid = idxThr;
        pthread_create(&v_thr[idxThr], NULL, processo_hijo, (void *) &v_reg[idxThr]);
    }

    //esperamos a que terminen
    for (idxThr=0; idxThr<NUM_THREADS; idxThr++) {
        pthread_join(v_thr[idxThr], NULL);
    }

    //desmontamos el sistema
    bumount(sistema_ficheros);

    return 0;
}


int recursive (int input)
{
    waitSem();
    char result = (input == 0) ? 0 : recursive(input-1);
    signalSem();
    return result;
}

void * processo_hijo(void * ptr)
{
    p_Registro r = (p_Registro) ptr;
    int i;
    for (i=0; i<NUM_THREADS; i++) {
	waitSem();
	printf("el número de thread es: %d\n", r->pid);
	recursive(r->pid);
	signalSem();
    }
    return ptr;
}
