/* definicion de los posibles errores */
#include "decl_basicas.h"

#define DESCRIPTOR                    -1   /* bloques.c				*/
#define OVERFLOW_INODOS               -2   /* reservar_inodo 			*/
#define OVERFLOW_BLOQUES              -3   /* reservar_bloque 			*/
#define VALOR_INCONSISTENTE           -4   /* escribir_bit, liberar_inodo	*/
#define NULL_BLOQUE_W                 -5   /* consultar_ruta 			*/
#define PERM_LECT                     -6
#define PERM_ESCR                     -7
#define PERM_EJEC                     -8
#define NO_EXISTE                     -9
#define YA_EXISTE                     -10
#define CAMINO_MAL_FORMADO            -11
#define TIPO_INODO_NO_ESPERADO        -12
#define NUMERO_PARAMETROS             -13
#define OVERFLOW_FICHERO              -14
#define CAMINO_INACCESIBLE            -15
/*
#define OVERFLOW_INODOS               -16
#define OVERFLOW_INODOS               -17
*/
#define NO_ENCONTRADO                 -17

int error (int t_error);
