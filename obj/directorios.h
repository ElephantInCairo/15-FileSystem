#include "iterador_camino.h"

struct entrada_s {
    unsigned int inodo;
    char nombre[60];
};

typedef struct entrada_s Entrada;
typedef struct entrada_s * p_Entrada;

//consulta Inodo
int obtener_inodo       (const char * camino, int * p_inodo, char t_inodo0);
int obtener_inodo_padre (const char * camino, int * p_inodo, char existe, char t_inodo0, unsigned int * p_entrada, char * nombre, int * p_inodo_hijo);

//funciones
int mi_create (const char * camino, unsigned char permisos, char t_inodo);
int mi_dir    (const char * camino, char * buffer);
int mi_link   (const char * camino1, const char * camino2);
int mi_unlink (const char * camino, char t_inodo);
int mi_chmod  (const char * camino, unsigned char permisos);
int mi_stat   (const char * camino, p_Stat stat);
int mi_read   (const char * camino,       void * buf, unsigned int offset, unsigned int nbytes);
int mi_write  (const char * camino, const void * buf, unsigned int offset, unsigned int nbytes);
