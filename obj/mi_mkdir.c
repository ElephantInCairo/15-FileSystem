#include "directorios.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/**
 * Primer argumento  (1): nombre del SF
 * Segundo argumento (2): permisos
 * Tercer argumento  (3): ruta
 */
int main(int argc, char *argv[]) {

    char * nombre_sf;
    unsigned char permisos;
    char * ruta;

    //confirmar que los argumentos estan bien
    if(argc != 4) {
        printf("Formato: %s nombre_sf permisos /ruta\n", argv[0]);
        return error(NUMERO_PARAMETROS);
    }

    //ponemos nombres legibles
    nombre_sf = argv[1];
    permisos  = atoi(argv[2]);
    ruta      = argv[3];

    //montamos el sistema de ficheros
    bmount(nombre_sf);

    mi_create(ruta, permisos, DIRECTORIO);

    //desmontamos el sistema de ficheros
    bumount();

    return 0;
}
