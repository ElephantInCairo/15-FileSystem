#include "directorios.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define TAM_BUFFER 2048


/**
 * Primer argumento  (1): nombre del SF
 * Segundo argumento (2): ruta
 */
int main(int argc, char *argv[]) {

    char * nombre_sf;
    char * camino;
    char buffer[TAM_BUFFER];
    int  bytesLeidos;
    unsigned int offset = 0;

    //confirmar que los argumentos estan bien
    if(argc != 3) {
        printf("Formato: %s nombre_sf /ruta_fichero\n", argv[0]);
        return error(NUMERO_PARAMETROS);
    }

    //ponemos nombres legibles
    nombre_sf = argv[1];
    camino    = argv[2];

    //montamos el sistema de ficheros
    bmount(nombre_sf);

    for ( offset=0; (bytesLeidos = mi_read(camino, (void*) buffer, offset, TAM_BUFFER)) > 0; offset += bytesLeidos )
        write(1, buffer, bytesLeidos);

    //desmontamos el sistema de ficheros
    bumount();

    return 0;
}
