#include "g_errores.h"

int error(int t_error) {

    printf("ERROR: ");
    switch(t_error)
    {
        case DESCRIPTOR:
	    printf("Sistema de ficheros inaccesible");
	    break;
        case OVERFLOW_INODOS:
	    printf("No quedan inodos libres");
	    break;
        case OVERFLOW_BLOQUES:
	    printf("No quedan bloques libres");
	    break;
        case OVERFLOW_FICHERO:
	    printf("Se ha excedido el límite del fichero");
	    break;
        case VALOR_INCONSISTENTE:
	    printf("La operación no cambia el estado");
	    break;
        case PERM_LECT:
	    printf("Intentando leer sin permiso de lectura");
	    break;
        case PERM_ESCR:
	    printf("Intentando escribir sin permiso de escritura");
	    break;
        case PERM_EJEC:
	    printf("No se puede abrir el directorio");
	    break;
        case CAMINO_INACCESIBLE:
	    printf("No se ha encontrado el directorio intermedio");
	    break;
        case NO_EXISTE:
	    printf("El directorio o fichero especificado no existe");
	    break;
        case YA_EXISTE:
	    printf("El directorio o fichero especificado ya existe");
	    break;
        case CAMINO_MAL_FORMADO:
	    printf("La ruta especificada no es válida");
	    break;
        case TIPO_INODO_NO_ESPERADO:
	    printf("El tipo del inodo no es el esperado");
	    break;
        case NUMERO_PARAMETROS:
	    printf("Sintaxis incorrecta");
	    break;
        default:
	    printf("Error no definido");
	    break;
    }
    printf("\n");
    return t_error;
}

/*
        case NULL_BLOQUE_ERROR:
	    printf("Intentando acceder a un bloque que no está reservado");
	    break;
 */
