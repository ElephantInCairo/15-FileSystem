#include <pthread.h>
#include "simulacion.h"

#define T_SCAMINO  1024
#define T_STIME     128

void print_reg     (int ninodo, int desplReg);
char es_valido_reg (p_Registro preg, int idxThr);

int main(int argc, char *argv[])
{
    char * sistema_ficheros;
    char * ruta;

    if (argc != 3) {
        printf("Formato: %s nombre_sf dir", argv[0]);
        return -1;
    }

    //ponemos nombres legibles
    sistema_ficheros = argv[1];
    ruta             = argv[2];

    //montamos el sistema
    bmount(sistema_ficheros);

    //var.
    char fichero[T_SCAMINO];
    char * p_fichero = fichero;
    int ninodo;
    Inodo inodo;
    Registro reg;

    //buscamos en NUM_THREADS ficheros
    int idxThr;
    for (idxThr=0; idxThr<NUM_THREADS; idxThr++) {

	//Init info fichero: camino, inodo, tam, valores pred.
	snprintf(p_fichero, T_SCAMINO, "%s/procesos_%d/prueba.dat", ruta, idxThr);
	ninodo = 0; obtener_inodo(fichero, &ninodo, FICHERO);

	printf("%s\n", fichero);

	inodo = leer_inodo(ninodo);
	unsigned int tam = inodo.tamEnBytesLog;

	int reg_val = 0;
	int min_pos = 0;
	int max_pos = 0;
	int min_escr = 0;
	int max_escr = 0;

	int idxReg;
	for (idxReg=tam-T_REGISTRO; idxReg>=0; idxReg-=T_REGISTRO) {
	    memset(&reg, 0, T_REGISTRO);
	    if (mi_read_f(ninodo, &reg, idxReg, T_REGISTRO) < 0) continue;
	    //print_reg(ninodo, idxReg);
	    //printf("desplReg: %d", idxReg);
	    if ( es_valido_reg(&reg, idxThr) ) {
		//printf("idxReg: %d\n", idxReg);
		reg_val++;
		min_pos = idxReg;
		if (reg.nescritura == 1 )       min_escr = idxReg;
		if (reg.nescritura == NUM_ESCR) max_escr = idxReg;
	    }
	}
	max_pos = tam-T_REGISTRO;

	printf("PROCESO:    %d\n", idxThr);
	printf("NUM_ESCRIT: %d\n", reg_val);
	printf("MIN_ESCR:"); print_reg(ninodo, min_escr);
	printf("MAX_ESCR:"); print_reg(ninodo, max_escr);
	printf("MIN_POS:"); print_reg(ninodo, min_pos);
	printf("MAX_POS:"); print_reg(ninodo, max_pos);
	printf("\n");
    }

    //desmontamos el sistema
    bumount(sistema_ficheros);

    return 0;
}

void print_reg(int ninodo, int desplReg)
{
    Registro reg;
    memset(&reg, 0, T_REGISTRO);
    mi_read_f(ninodo, &reg, desplReg, T_REGISTRO);

    char s_time[T_STIME];
    strftime(s_time, T_STIME, "%Y-%m-%d %H:%M:%S", localtime(&reg.fecha));

    printf("\n");
    printf("fecha_escritura:  %s\n", s_time);
    printf("número_escritura: %d\n", reg.nescritura);
    printf("número_posicion:  %d\n", reg.pos);
}

char es_valido_reg (p_Registro preg, int idxThr)
{
    return (preg->nescritura>0 && preg->nescritura<=NUM_ESCR
	    && preg->pid==idxThr && preg->fecha>0);
}
