#include "iterador_camino.h"

void primero (const char * camino, p_Iterador_Camino pit) { *pit = 0; }

/**
 * TRUE: Si queda "\0" o "/\0" aka. estoy a final de cadena
 *  y puede haber o no una barra (/)
 */
char es_cam_val (const char * camino, p_Iterador_Camino pit)
{
    unsigned int idx = *pit;
    if (camino[idx]=='\0') return FALSE;
    return camino[idx+1]!='\0';
}

/**
 * TRUE: Si queda más de un nombre sobre el que iterar
 */
char es_dir_padre_valido (const char * camino, p_Iterador_Camino pit)
{
    unsigned int idx = *pit;
    if (camino[idx]  =='\0') return FALSE;
    if (camino[idx+1]=='\0') return FALSE;

    unsigned int despl = 0;
    unsigned char ndir = 0;
    for(; camino[despl+idx] != '\0' && ndir<2; despl++)
        if (camino[despl+idx] == '/') ndir++;

    return camino[despl+idx] != '\0';
}

/**
 * prec: el iterador apunta a alguna / de el camino
 */
void cons_and_sig (const char * camino, p_Iterador_Camino pit, char * nombre, char * p_tipo)
{
    Iterador_Camino itn = 0;
    for ((*pit)++; camino[*pit]!='/' && camino[*pit]!='\0'; (*pit)++)
        nombre[itn++] = camino[*pit];
    nombre[itn] = '\0'; //null-terminated

    *p_tipo = (camino[*pit]=='/') ? DIRECTORIO : FICHERO;
}
