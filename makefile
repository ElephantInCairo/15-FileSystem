CC=gcc
CFLAGS=-Wall -g -pthread
#CFLAGS=-Wall


#librerias/includes
#IDIR=include
_DEPS=simulacion.h directorios.h iterador_camino.h ficheros.h iterador_inodo.h ficheros_basico.h bloques.h g_errores.h semaforo_mutex_posix.h
DEPS=$(patsubst %,$(IDIR)/%,$(_DEPS))

#subdiectorio de código
ODIR=obj
_OBJ=semaforo_mutex_posix.o g_errores.o bloques.o ficheros_basico.o iterador_inodo.o ficheros.o iterador_camino.o directorios.o
OBJ=$(patsubst %,$(ODIR)/%,$(_OBJ))

BIN=./bin

#nombre de los ejecutables
all : etapa1 etapa2 tests entrega_final
	@echo "Compilación de: $^"


#paquetes
etapa1: escribir leer leer_SF mi_mkfs
	@echo "Compilación de: $^\n"

etapa2: mi_mkdir mi_touch mi_chmod mi_ls mi_stat mi_escribir mi_cat mi_ln mi_rm mi_rmdir
	@echo "Compilación de: $^\n"

tests: test test_reentrant_lock unlock
	@echo "Compilación de: $^\n"

entrega_final: simulacion verificacion
	@echo "Compilación de: $^\n"



#después de un clean
$(ODIR)/%.o: %.c $(DEPS)
	$(CC) $(CFLAGS) -c -o $(BIN)/$@ $<


#compilación de cada ejecutable
test: $(OBJ) $(ODIR)/full_test.o
	$(CC) $(CFLAGS) -o $(BIN)/$@ $^

test_dir: $(OBJ) $(ODIR)/test_dir.o
	$(CC) $(CFLAGS) -o $(BIN)/$@ $^

escribir: $(OBJ) $(ODIR)/escribir.o
	$(CC) $(CFLAGS) -o $(BIN)/$@ $^

leer: $(OBJ) $(ODIR)/leer.o
	$(CC) $(CFLAGS) -o $(BIN)/$@ $^

leer_SF: $(OBJ) $(ODIR)/leer_SF.o
	$(CC) $(CFLAGS) -o $(BIN)/$@ $^

mi_mkfs: $(OBJ) $(ODIR)/mi_mkfs.o
	$(CC) $(CFLAGS) -o $(BIN)/$@ $^

mi_mkdir: $(OBJ) $(ODIR)/mi_mkdir.o
	$(CC) $(CFLAGS) -o $(BIN)/$@ $^

mi_touch: $(OBJ) $(ODIR)/mi_touch.o
	$(CC) $(CFLAGS) -o $(BIN)/$@ $^

mi_ls: $(OBJ) $(ODIR)/mi_ls.o
	$(CC) $(CFLAGS) -o $(BIN)/$@ $^

mi_chmod: $(OBJ) $(ODIR)/mi_chmod.o
	$(CC) $(CFLAGS) -o $(BIN)/$@ $^

mi_cat: $(OBJ) $(ODIR)/mi_cat.o
	$(CC) $(CFLAGS) -o $(BIN)/$@ $^

mi_rmdir: $(OBJ) $(ODIR)/mi_rmdir.o
	$(CC) $(CFLAGS) -o $(BIN)/$@ $^

mi_rm: $(OBJ) $(ODIR)/mi_rm.o
	$(CC) $(CFLAGS) -o $(BIN)/$@ $^

mi_stat: $(OBJ) $(ODIR)/mi_stat.o
	$(CC) $(CFLAGS) -o $(BIN)/$@ $^

mi_ln: $(OBJ) $(ODIR)/mi_ln.o
	$(CC) $(CFLAGS) -o $(BIN)/$@ $^

mi_escribir: $(OBJ) $(ODIR)/mi_escribir.o
	$(CC) $(CFLAGS) -o $(BIN)/$@ $^

simulacion: $(OBJ) $(ODIR)/simulacion.o
	$(CC) $(CFLAGS) -o $(BIN)/$@ $^

verificacion: $(OBJ) $(ODIR)/verificacion.o
	$(CC) $(CFLAGS) -o $(BIN)/$@ $^

testSem: $(OBJ) $(ODIR)/testSem.o
	$(CC) $(CFLAGS) -o $(BIN)/$@ $^

test_reentrant_lock: $(ODIR)/bloques.o $(ODIR)/semaforo_mutex_posix.o $(ODIR)/test_lock.c
	$(CC) $(CFLAGS) -o $(BIN)/$@ $^

unlock: $(ODIR)/semaforo_mutex_posix.o $(ODIR)/unlock.o
	$(CC) $(CFLAGS) -o $(BIN)/$@ $^

#limpiar los .o
.PHONY: clean
clean:
	rm $(ODIR)/*.o

