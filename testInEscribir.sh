# TESTS escribir en bloques enteros, desplazamientos, etc.
./escribir ha 0 "offset 0, bloque 0"
./escribir ha 800 "$(cat ent1.txt)"
./escribir ha 1024 "$(cat ent2.txt)"
./escribir ha 2000 "$(cat ent3.txt)"

# TESTS escribir en diferentes bloques lógicos
./escribir ha 700 "offset 700, bloque 0"
./escribir ha 5120 "offset 5120, bloque 5"
./escribir ha 256000 "offset 256000, bloque 250"
./escribir ha 30720000 "offset 30720000, bloque 30000"
./escribir ha 71680000 "offset 71680000, bloque 70000"
./escribir ha 71680000 "$(cat obj/ficheros.c)"

./escribir ha 12280 "cambio de árbol dir->indir"


# VALORES ESPERADOS
#
#  1: tamaño = 18 bytes,	primer bloque		desde el inicio
#  2: tamaño = 1312 bytes,	primer y segundo bloque	con despl
#  3: tamaño = 2048 bytes,	segundo bloque compl	bloque entero y justo
#  4: tamaño = 3536 bytes,	seg, terc, cuart bloq	despl, entero, despl
#
#  5: bloque = 0	directos
#  6: bloque = 5	directos
#  7: bloque = 250	indirecto 1
#  8: bloque = 30000	indirecto 2
#  9: bloque = 70000	indirecto 3
# 10: bloque = 70000, más de un bloque
