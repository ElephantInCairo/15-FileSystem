./mi_mkfs ha 204800

#creamos ficheros suficientes para que el directorio ocupe mas de un bloque
echo "Creamos 18 archivos"
./mi_touch ha 7 /file1
./mi_touch ha 7 /file2
./mi_touch ha 7 /file3
./mi_touch ha 7 /file4
./mi_touch ha 7 /file5
./mi_touch ha 7 /file6
./mi_touch ha 7 /file7
./mi_touch ha 7 /file8
./mi_touch ha 7 /file9
./mi_touch ha 7 /file10
./mi_touch ha 7 /file11
./mi_touch ha 7 /file12
./mi_touch ha 7 /file13
./mi_touch ha 7 /file14
./mi_touch ha 7 /file15
./mi_touch ha 7 /file16
./mi_touch ha 7 /file17
./mi_touch ha 7 /file18
./mi_ls    ha /
echo ""

echo "Stat de /"
./mi_stat ha /
echo ""

#sigue ocupando más de un bloque
echo "Eliminamos /file3"
./mi_rm ha /file3
./mi_ls ha /
echo ""

#se ha truncado el último bloque
echo "Eliminamos /file18"
./mi_rm ha /file18
./mi_ls ha /
echo ""
